#include "server.h"

static void Prepause(GstRTSPClient *self, GstRTSPContext *ctx, gpointer user_data) 
{
    printf("\n\nPrepause\n\n");
}

static void ClientConnected(GstRTSPServer* gstrtspserver, GstRTSPClient* arg1, gpointer user_data)
{
    printf("Client connected\n");
    GstRTSPConnection* connection = gst_rtsp_client_get_connection(arg1);
    if (!connection) {
        return;
    }

    gulong clientConnectedConfigureId = g_signal_connect(arg1, "pre-pause-request", (GCallback)Prepause, NULL);

    //gst_rtsp_connection_set_tunneled(connection, true);
    //gst_rtsp_connection_set_http_mode(connection, true);
}

RtspServer* RtspServer::instance_ = nullptr;

bool RtspServer::Initialize(char *port, char *username, char *password) 
{
    printf("rtsp port: %s\n",port);
    printf("username: %s\n",username);
    printf("password: %s\n",password);
  
    gst_init(NULL, NULL);

    serverctx.loop = g_main_loop_new(NULL, FALSE);

    serverctx.server = gst_rtsp_server_new();
    serverctx.mounts = gst_rtsp_server_get_mount_points(serverctx.server);
    gst_rtsp_server_set_address(serverctx.server, serverctx.config->address.c_str());
    //gst_rtsp_server_set_service(serverctx.server, serverctx.config->port.c_str());
    gst_rtsp_server_set_service(serverctx.server, port);
    auto&& session = gst_rtsp_session_new("New session");
    gst_rtsp_session_prevent_expire(session);

    serverctx.factory = gst_rtsp_media_factory_new();

    gst_rtsp_media_factory_set_shared(serverctx.factory, TRUE);
    gst_rtsp_media_factory_set_latency(serverctx.factory, 0);
    gst_rtsp_mount_points_add_factory(serverctx.mounts, serverctx.config->route.c_str(), serverctx.factory);
    g_object_unref(serverctx.mounts);

    gulong clientConnectedConfigureId = g_signal_connect(serverctx.server, "client-connected", (GCallback)ClientConnected, this);

    /* Pipeline launch */
    std::string launchCmd = "";
#if 1
    launchCmd += "alarmsrc connection=0 ";
    //launchCmd += " ! queue ! h264parse ! rtph264pay name=pay0 pt=96";
    launchCmd += " ! queue ! h264parse ! rtph264pay name=pay0 pt=96 alarmsrc connection=1 ! fakesink";
#else
    launchCmd += "videotestsrc "; 
    std::cout << "Resolution:\t" << 1280 << "x"
      << 720 << std::endl
      << std::endl;
    launchCmd += " ! videoscale ! video/x-raw,width=";
    launchCmd += "1280";
    launchCmd += ",height=";
    launchCmd += "720";
    launchCmd += " ! capsfilter ! queue ! openh264enc";
    launchCmd += " ! rtph264pay name=pay0 pt=96";
#endif

// change to 1 to enable audio; 0 to do audio test src
#if 0
    launchCmd += " alarmsrc connection=3 ! queue ! rtppcmupay name=pay1 pt=0";
#else    
    launchCmd += " audiotestsrc ! audioconvert ! audioresample ! queue ! audio/x-raw, rate=8000, channels=1 ! mulawenc ! queue ! rtppcmupay name=pay1 pt=0";
#endif

    g_print("Launching stream with the following pipeline: %s\n", launchCmd.c_str());
    gst_rtsp_media_factory_set_launch(serverctx.factory, launchCmd.c_str());

    gst_rtsp_media_factory_add_role(serverctx.factory, username, 
        GST_RTSP_PERM_MEDIA_FACTORY_ACCESS, G_TYPE_BOOLEAN, TRUE,
        GST_RTSP_PERM_MEDIA_FACTORY_CONSTRUCT, G_TYPE_BOOLEAN, TRUE, NULL);

    GstRTSPAuth* auth;
    GstRTSPToken* token;
    
    auth = gst_rtsp_auth_new();
    gst_rtsp_auth_set_supported_methods(auth, GST_RTSP_AUTH_DIGEST);
    token = gst_rtsp_token_new(GST_RTSP_TOKEN_MEDIA_FACTORY_ROLE, G_TYPE_STRING, username, NULL);
    gst_rtsp_auth_add_digest(auth, username, password, token);
    gst_rtsp_token_unref(token);

    //gst_rtsp_server_set_auth(serverctx.server, auth);
    gst_rtsp_server_set_auth(serverctx.server, NULL);
    g_object_unref(auth);

    if (gst_rtsp_server_attach(serverctx.server, NULL) == 0) {
        return false;
    }

    g_main_loop_run(serverctx.loop);

    return true;
}

void RtspServer::SetDefaults() 
{
    serverctx.config = std::make_shared<ServerConfig>();
    serverctx.config->usrname = "";
    serverctx.config->pass = "";
    serverctx.config->route = "/api/GetStream";
    serverctx.config->address = "0.0.0.0";
    serverctx.config->port = "8554";
    serverctx.config->input = "";
    serverctx.config->source_type = SourceType::RTSP_SOURCE;
}
