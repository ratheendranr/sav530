/*************************************************************************************
* @file stats.h
*
* app statistics
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#ifndef STATS_H
#define STATS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <glib.h>
#include <glib/gstdio.h>

/**
 * @brief result as enum for AppConfig methods
 */
typedef enum {
    STATS_RESULT_OK = 0,                   //!< api call was successful
    STATS_RESULT_ERR_MEM_ALLOC,            //!< memory allocation failure
    STATS_RESULT_ERR_BAD_JSON,             //!< json object is not valid
    STATS_RESULT_ERR_JSON_OBJECT,          //!< could not create json object
    STATS_RESULT_ERR_JSON_FILE,            //!< could not create json file
    STATS_RESULT_ERR_UNKNOWN_STAT_TYPE     //!< unknown stat type
} StatsResult;

typedef struct _Stats Stats;

extern void stats_increment_total_sessions(Stats* stats);
extern void stats_increment_auth_failures(Stats* stats);
extern void stats_increment_ss_connects(Stats* stats);
extern void stats_increment_ss_disconnects(Stats* stats);

extern Stats* stats_new(GMainContext* ctx, gchar const* output_file, guint interval);
extern void stats_destroy(Stats** self_p);

#ifdef __cplusplus
}
#endif

#endif /* STATS_H */
