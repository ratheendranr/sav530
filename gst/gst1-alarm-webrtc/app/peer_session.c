/*************************************************************************************
* @file peer_session.c
*
* This class handles peer session information.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include <openssl/hmac.h>
#include <glib/gprintf.h>

#include "uri_encode.h"
#include "peer_session.h"

struct _IceConfiguration {
    gchar stun_url[128];
    gchar turn_url[128];
    gchar username[64];
    gchar credential[64];
    gchar stun_address[256];
    gchar turn_address[256];
};

struct _PeerSession {
    gchar* peer_id;
    gchar auth[256];
    struct _IceConfiguration ice;
};

static void s_for_each_ice_server_element(JsonArray *array, guint index,
        JsonNode *element_node, gpointer user_data)
{
    PeerSession *session = (PeerSession*) user_data;
    if (session) {
        JsonObject *json_obj = json_node_get_object(element_node);
        JsonNode *node = NULL;
        if (json_object_has_member(json_obj, "username")) {
            /* turn server info */
            g_strlcpy(session->ice.username,
                    json_object_get_string_member(json_obj, "username"),
                    sizeof(session->ice.username));

            if (json_object_has_member(json_obj, "credential")) {
                gchar const* cred = json_object_get_string_member(json_obj, "credential");
                if (cred) {
                    gsize input_size = strlen(cred) * 3 + 1;
                    gchar buffer[input_size];
                    memset(buffer, 0, sizeof(buffer));
                    uri_encode(cred, strlen(cred), buffer);
                    g_strlcpy(session->ice.credential, buffer,
                            sizeof(session->ice.credential));
                }
            }

            JsonArray *array = (JsonArray*) json_object_get_array_member(
                    json_obj, "urls");
            /* TODO - shouldn't ever be more than one item in this array, but
             * should handle more than one case anyway
             */

            node = json_array_get_element(array, 0);
            if (node) {
                g_strlcpy(session->ice.turn_url, strstr(json_node_get_string(node), ":")+1,
                        sizeof(session->ice.turn_url));
            }
        } else {
            JsonArray *array = (JsonArray*) json_object_get_array_member(
                    json_obj, "urls");
            /* TODO - shouldn't ever be more than one item in this array, but
             * should handle more than one case anyway
             */
            node = json_array_get_element(array, 0);
            if (node) {
                g_strlcpy(session->ice.stun_url, strstr(json_node_get_string(node), ":")+1,
                        sizeof(session->ice.stun_url));
            }
        }
    }
}

static guchar* s_hmac_sha256(const gpointer *key, gint keylen,
        const guchar *data, gint datalen,
        guchar *result, guint* resultlen)
{
    return HMAC(EVP_sha256(), key, keylen, data, datalen, result, resultlen);
}

/*********************************************************************/
/******************************** API ********************************/
/*********************************************************************/
gchar const* peer_session_result_to_string(PeerSessionResult result)
{
    return  result == PEER_SESSION_RESULT_OK ?                        "OK" :
            result == PEER_SESSION_RESULT_ERR_MEM_ALLOC ?             "ERR_MEM_ALLOC" :
            result == PEER_SESSION_RESULT_ERR_FAILED_TO_PARSE ?       "ERR_FAILED_TO_PARSE" :
            result == PEER_SESSION_RESULT_ERR_MISSING_FIELD ?         "ERR_MISSING_FIELD" :
                                                                      "UNKNOWN";
}

gchar const* peer_session_get_ice_stun_url(PeerSession const* session)
{
    return session->ice.stun_url;
}

gchar const* peer_session_get_ice_turn_url(PeerSession const* session)
{
    return session->ice.turn_url;
}

gchar const* peer_session_get_ice_username(PeerSession const* session)
{
    return session->ice.username;
}

gchar const* peer_session_get_ice_credential(PeerSession const* session)
{
    return session->ice.credential;
}

gchar const* peer_session_get_ice_stun_server(PeerSession const* session)
{
    return session->ice.stun_address;
}

gchar const* peer_session_get_ice_turn_server(PeerSession const* session)
{
    return session->ice.turn_address;
}

gchar const* peer_session_get_peer_id(PeerSession const* session)
{
    return session->peer_id;
}

gboolean peer_session_valid_auth(PeerSession const* session, guchar const* secret)
{
    guchar result[128] = {0};
    guint result_len = 0;
    gchar result_hexstring[128] = {0};
    gint i;
    gchar** tokens = NULL;

    if (!secret)
        return FALSE;

    tokens = g_strsplit(session->auth, ":", -1);
    if (!tokens)
        return FALSE;

    gint64 peer_time = g_ascii_strtoll(tokens[0], NULL, 10);
    GDateTime* gd_this = g_date_time_new_now_utc();
    gint64 this_time = g_date_time_to_unix(gd_this);
    g_date_time_unref(gd_this);

    /* verify peer timestamp hasn't expired */
    if (peer_time < this_time - 180) {
        g_strfreev(tokens);
        return FALSE;
    }

    s_hmac_sha256((gpointer const*)secret, strlen((gchar*)secret), (guchar const*)tokens[0], strlen(tokens[0]), result, &result_len);

    for (i = 0; i < result_len; ++i) {
        g_sprintf(&(result_hexstring[i*2]), "%02x", result[i]);
    }

    if (g_strcmp0(result_hexstring, tokens[1]) != 0) {
        g_strfreev(tokens);
        return FALSE;
    }

    g_strfreev(tokens);

    return TRUE;
}

PeerSessionResult peer_session_parse(PeerSession *session, char const *json_str)
{
    JsonNode *root = NULL;
    JsonObject *object = NULL;
    JsonParser *parser = json_parser_new();

    if (!json_parser_load_from_data(parser, json_str, -1, NULL)) {
        g_object_unref(parser);
        return PEER_SESSION_RESULT_ERR_FAILED_TO_PARSE;
    }

    root = json_parser_get_root(parser);
    if (!JSON_NODE_HOLDS_OBJECT(root)) {
        g_object_unref(parser);
        return PEER_SESSION_RESULT_ERR_BAD_JSON;
    }

    object = json_node_get_object(root);

    if (json_object_has_member(object, "watcher_peer_id")) {
        session->peer_id = g_strdup(
                json_object_get_string_member(object, "watcher_peer_id"));
    } else {
        g_object_unref(parser);
        return PEER_SESSION_RESULT_ERR_MISSING_FIELD;
    }

    if (json_object_has_member(object, "auth")) {
        g_strlcpy(session->auth,
                json_object_get_string_member(object, "auth"),
                sizeof(session->auth));
    } else {
        g_object_unref(parser);
        return PEER_SESSION_RESULT_ERR_MISSING_FIELD;
    }

    if (json_object_has_member(object, "iceServers")) {
        JsonArray *array = (JsonArray*) json_object_get_array_member(object,
                "iceServers");
        json_array_foreach_element(array, s_for_each_ice_server_element,
                session);

        g_snprintf(session->ice.stun_address, sizeof(session->ice.stun_address),
                "stun://%s", session->ice.stun_url);
        g_snprintf(session->ice.turn_address, sizeof(session->ice.turn_address),
                "turn://%s:%s@%s", session->ice.username,
                session->ice.credential, session->ice.turn_url);

    } else {
        g_object_unref(parser);
        return PEER_SESSION_RESULT_ERR_MISSING_FIELD;
    }

    g_object_unref(parser);
    return PEER_SESSION_RESULT_OK;
}

PeerSession* peer_session_new(void)
{
    return g_malloc0(sizeof(PeerSession));
}

void peer_session_destroy(PeerSession** self_p)
{
    if (self_p) {
        PeerSession* self = *self_p;
        if (self) {
            if (self->peer_id)
                g_free(self->peer_id);
            g_free(self);
            *self_p = NULL;
        }
    }
}
