/*************************************************************************************
* @file webrtc_gst.h
*
* This source handles webrtc gst related functions.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#ifndef WEBRTC_GST_H
#define WEBRTC_GST_H

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef GST_USE_UNSTABLE_API
#define GST_USE_UNSTABLE_API
#endif
#include <gst/webrtc/webrtc_fwd.h>

/**
 * @brief return string version of bundle policy enum
 * @param policy GstWebRTCBundlePolicy enum
 * @return string bundle policy
 */
gchar const* webrtc_gst_bundle_policy_enum_to_string(GstWebRTCBundlePolicy policy);

/**
 * @brief convert bundle policy string to enum
 * @param policy string
 * @return GstWebRTCBundlePolicy enum
 */
GstWebRTCBundlePolicy webrtc_gst_bundle_policy_string_to_enum(gchar const* policy);

/**
 * @brief return string version of ice transport policy enum
 * @param policy GstWebRTCICETransportPolicy enum
 * @return string ice transport policy
 */
gchar const* webrtc_gst_ice_transport_policy_enum_to_string(GstWebRTCICETransportPolicy policy);

/**
 * @brief convert bundle policy string to enum
 * @param policy string
 * @return GstWebRTCICETransportPolicy enum
 */
GstWebRTCICETransportPolicy webrtc_gst_ice_transport_policy_string_to_enum(gchar const* policy);


#ifdef __cplusplus
}
#endif

#endif /* WEBRTC_GST_H */
