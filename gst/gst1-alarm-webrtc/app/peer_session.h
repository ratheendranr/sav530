/*************************************************************************************
* @file peer_session.h
*
* This class handles peer session information.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#ifndef PEER_SESSION_H
#define PEER_SESSION_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <json-glib/json-glib.h>

/**
 * @brief result as enum for PeerSession methods
 */
typedef enum {
    PEER_SESSION_RESULT_OK = 0,             //!< api call was successful
    PEER_SESSION_RESULT_ERR_MEM_ALLOC,      //!< memory allocation failure
    PEER_SESSION_RESULT_ERR_FAILED_TO_PARSE,//!< failed to parse
    PEER_SESSION_RESULT_ERR_BAD_JSON,       //!< json object is not valid
    PEER_SESSION_RESULT_ERR_MISSING_FIELD   //!< missing field
} PeerSessionResult;

typedef struct _PeerSession PeerSession;

/**
 * returns result enum as string
 * @param result PeerSessionResult enum
 * @return string version of result value
 */
extern gchar const* peer_session_result_to_string(PeerSessionResult result);

/**
 * @brief get ice stun url
 * @param session PeerSession object
 * @return string stun url
 */
extern gchar const* peer_session_get_ice_stun_url(PeerSession const* session);

/**
 * @brief get ice turn url
 * @param session PeerSession object
 * @return string turn url
 */
extern gchar const* peer_session_get_ice_turn_url(PeerSession const* session);

/**
 * @brief get ice username
 * @param session PeerSession object
 * @return string username
 */
extern gchar const* peer_session_get_ice_username(PeerSession const* session);

/**
 * @brief get ice credential
 * @param session PeerSession object
 * @return string credential
 */
extern gchar const* peer_session_get_ice_credential(PeerSession const* session);

/**
 * @brief get full ice stun server address
 * @param session PeerSession object
 * @return string ice stun server address
 */
extern gchar const* peer_session_get_ice_stun_server(PeerSession const* session);

/**
 * @brief get full ice turn server address
 * @param session PeerSession object
 * @return string ice turn server address
 */
extern gchar const* peer_session_get_ice_turn_server(PeerSession const* session);

/**
 * @brief get peer id
 * @param session PeerSession object
 * @return string peer id
 */
extern gchar const* peer_session_get_peer_id(PeerSession const* session);

/**
 * @brief validate auth token
 * @param session PeerSession object
 * @param secret webrtc secret
 * @return TRUE if valid, otherwise FALSE
 *
 * This method creates a sha256 hmac and validates the auth token received
 * by the signalling server. The webrtc secret is supplied at installation
 * time.
 *
 */
extern gboolean peer_session_valid_auth(PeerSession const* session, guchar const* secret);

/**
 * @brief parse the json message from signalling server
 * @param session PeerSession object
 * @param json_str string containing json message
 * @return PEER_SESSION_RESULT_OK if success, otherwise failure
 */
extern PeerSessionResult peer_session_parse(PeerSession* session, char const* json_str);

/**
 * @brief create PeerSession object
 * @return PeerSession object if success, otherwise NULL for failure
 */
extern PeerSession* peer_session_new(void);

/**
 * @brief destroys a PeerSession object and sets ptr to NULL
 * @param session_p pointer to PeerSession object
 */
extern void peer_session_destroy(PeerSession** session_p);

#ifdef __cplusplus
}
#endif

#endif /* PEER_SESSION_H */
