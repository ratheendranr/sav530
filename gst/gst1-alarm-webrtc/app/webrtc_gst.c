/*************************************************************************************
* @file webrtc_gst.c
*
* This source handles webrtc gst related functions.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include "webrtc_gst.h"

#define WEBRTC_GST_BUNDLE_POLICY_DEFAULT        (GST_WEBRTC_BUNDLE_POLICY_MAX_BUNDLE)
#define WEBRTC_GST_BUNDLE_POLICY_NONE           "none"
#define WEBRTC_GST_BUNDLE_POLICY_BALANCED       "balanced"
#define WEBRTC_GST_BUNDLE_POLICY_MAX_COMPAT     "max-compat"
#define WEBRTC_GST_BUNDLE_POLICY_MAX_BUNDLE     "max-bundle"

#define WEBRTC_GST_ICE_TRANSPORT_POLICY_DEFAULT (GST_WEBRTC_ICE_TRANSPORT_POLICY_ALL)
#define WEBRTC_GST_ICE_TRANSPORT_POLICY_ALL     "all"
#define WEBRTC_GST_ICE_TRANSPORT_POLICY_RELAY   "relay"

/*********************************************************************/
/******************************** API ********************************/
/*********************************************************************/
gchar const* webrtc_gst_bundle_policy_enum_to_string(GstWebRTCBundlePolicy policy)
{
    switch (policy) {
    case GST_WEBRTC_BUNDLE_POLICY_NONE:
        return WEBRTC_GST_BUNDLE_POLICY_NONE;
    case GST_WEBRTC_BUNDLE_POLICY_BALANCED:
        return WEBRTC_GST_BUNDLE_POLICY_BALANCED;
    case GST_WEBRTC_BUNDLE_POLICY_MAX_COMPAT:
        return WEBRTC_GST_BUNDLE_POLICY_MAX_COMPAT;
    case GST_WEBRTC_BUNDLE_POLICY_MAX_BUNDLE:
        return WEBRTC_GST_BUNDLE_POLICY_MAX_BUNDLE;
    default:
        return NULL;
    }
}

GstWebRTCBundlePolicy webrtc_gst_bundle_policy_string_to_enum(gchar const* policy)
{
    if (!policy)
        return WEBRTC_GST_BUNDLE_POLICY_DEFAULT;
    else {
        if (g_ascii_strncasecmp(policy, WEBRTC_GST_BUNDLE_POLICY_NONE,
                strlen(WEBRTC_GST_BUNDLE_POLICY_NONE)) == 0)
            return GST_WEBRTC_BUNDLE_POLICY_NONE;
        else if (g_ascii_strncasecmp(policy, WEBRTC_GST_BUNDLE_POLICY_BALANCED,
                strlen(WEBRTC_GST_BUNDLE_POLICY_BALANCED)) == 0)
            return GST_WEBRTC_BUNDLE_POLICY_BALANCED;
        else if (g_ascii_strncasecmp(policy, WEBRTC_GST_BUNDLE_POLICY_MAX_COMPAT,
                strlen(WEBRTC_GST_BUNDLE_POLICY_MAX_COMPAT)) == 0)
            return GST_WEBRTC_BUNDLE_POLICY_MAX_COMPAT;
        else if (g_ascii_strncasecmp(policy, WEBRTC_GST_BUNDLE_POLICY_MAX_BUNDLE,
                strlen(WEBRTC_GST_BUNDLE_POLICY_MAX_BUNDLE)) == 0)
            return GST_WEBRTC_BUNDLE_POLICY_MAX_BUNDLE;
        else
            return WEBRTC_GST_BUNDLE_POLICY_DEFAULT;

    }
}

gchar const* webrtc_gst_ice_transport_policy_enum_to_string(GstWebRTCICETransportPolicy policy)
{
    switch (policy) {
    case GST_WEBRTC_ICE_TRANSPORT_POLICY_ALL:
        return WEBRTC_GST_ICE_TRANSPORT_POLICY_ALL;
    case GST_WEBRTC_ICE_TRANSPORT_POLICY_RELAY:
        return WEBRTC_GST_ICE_TRANSPORT_POLICY_RELAY;
    default:
        return NULL;
    }
}

GstWebRTCICETransportPolicy webrtc_gst_ice_transport_policy_string_to_enum(gchar const* policy)
{
    if (!policy)
        return WEBRTC_GST_ICE_TRANSPORT_POLICY_DEFAULT;
    else {
        if (g_ascii_strncasecmp(policy, WEBRTC_GST_ICE_TRANSPORT_POLICY_RELAY,
                strlen(WEBRTC_GST_ICE_TRANSPORT_POLICY_RELAY)) == 0)
            return GST_WEBRTC_ICE_TRANSPORT_POLICY_RELAY;
        else
            return WEBRTC_GST_ICE_TRANSPORT_POLICY_DEFAULT;

    }
}
