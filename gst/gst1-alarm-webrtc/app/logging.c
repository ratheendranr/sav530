/*************************************************************************************
* @file logging.c
*
* app log handlers
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include "logging.h"

#include <syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

void
syslog_gst_debug_log(GstDebugCategory *category, GstDebugLevel level,
        const gchar *file, const gchar *function, gint line, GObject *object,
        GstDebugMessage *message, gpointer user_data)
{
    int syslog_level = -1;

    openlog("webrtc", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_SYSLOG);

    switch (level) {
    case GST_LEVEL_ERROR:
        syslog_level = LOG_ERR;
        break;
    case GST_LEVEL_WARNING:
        syslog_level = LOG_WARNING;
        break;
    case GST_LEVEL_INFO:
        syslog_level = LOG_INFO;
        break;
    case GST_LEVEL_DEBUG:
        syslog_level = LOG_DEBUG;
        break;
    default:
        /* unused */
        break;
    }

    if (syslog_level != -1) {
        syslog(syslog_level, gst_debug_message_get(message));
    }

    closelog();
}

void
klog_gst_debug_log(GstDebugCategory *category, GstDebugLevel level,
        const gchar *file, const gchar *function, gint line, GObject *object,
        GstDebugMessage *message, gpointer user_data)
{
    GDateTime* gd_now = g_date_time_new_now_utc();
    char buffer[2048];    //placing buffer on stack for speed
    char level_str[16];
    int fd = -1;

    if (!gd_now)
        return;

    fd = open("/dev/kmsg", O_RDWR);
    if (fd < 0) {
        g_date_time_unref(gd_now);
        return;
    }

    switch (level) {
    case GST_LEVEL_ERROR:
        g_snprintf(level_str, sizeof(level_str), "ERROR");
        break;
    case GST_LEVEL_WARNING:
        g_snprintf(level_str, sizeof(level_str), "WARN");
        break;
    case GST_LEVEL_INFO:
        g_snprintf(level_str, sizeof(level_str), "INFO");
        break;
    case GST_LEVEL_DEBUG:
        g_snprintf(level_str, sizeof(level_str), "DEBUG");
        break;
    default:
        /* unused */
        break;
    }

    g_snprintf(buffer, sizeof(buffer),
            "%04d-%02d-%02d %02d:%02d:%02d %-7s [%s:%d] webrtc %s\n",
            g_date_time_get_year(gd_now), g_date_time_get_month(gd_now),
            g_date_time_get_day_of_month(gd_now), g_date_time_get_hour(gd_now),
            g_date_time_get_minute(gd_now), g_date_time_get_second(gd_now),
            level_str, function, line, gst_debug_message_get(message));
    g_date_time_unref(gd_now);

    write(fd, buffer, strlen(buffer));
    close(fd);
}
