/*************************************************************************************
* @file app_config.h
*
* This class handles reading, parsing, and writing to the JSON configuration file
* that this application requires for operation.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#ifndef APP_CONFIG_H
#define APP_CONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <glib.h>

#define APP_CONFIG_SCHEMA_VERSION_MAJOR 0
#define APP_CONFIG_SCHEMA_VERSION_MINOR 0
#define APP_CONFIG_SCHEMA_VERSION_PATCH 3
#define APP_CONFIG_SCHEMA_VERSION "0.0.3"

/**
 * @brief result as enum for AppConfig methods
 */
typedef enum {
    CONFIG_RESULT_OK = 0,                   //!< api call was successful
    CONFIG_RESULT_ERR_MEM_ALLOC,            //!< memory allocation failure
    CONFIG_RESULT_ERR_MISSING_FILE,         //!< configuration file does not exist
    CONFIG_RESULT_ERR_FAILED_TO_PARSE,      //!< failed to parse configuration file
    CONFIG_RESULT_ERR_BAD_JSON,             //!< json object is not valid
    CONFIG_RESULT_ERR_MISSING_FIELD,        //!< missing field in configuration file
    CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE,  //!< config type error with parsing into AppConfig struct
    CONFIG_RESULT_ERR_JSON_OBJECT,          //!< could not create json object
    CONFIG_RESULT_ERR_JSON_FILE             //!< could not create json file
} AppConfigResult;

typedef struct
{
    gchar *auth_secret;
    gchar *server_url;
    gchar *access_token;
    gint audio_send;
    gint audio_recv;
    gint video_stream_id;
    gint verbose;
    gchar *config_file_path;
    gboolean reset;
} CmdArgs;

/**
 * @brief libsoup configuration
 */
typedef struct
{
    gboolean strict_ssl;
    gboolean logging;
} SoupConfig;

/**
 * @brief webrtc specific configuration
 */
typedef struct
{
    gchar ice_transport_policy[32];
    gchar bundle_policy[32];
    gchar network_interface[32];
} WebrtcConfig;

/**
 * @brief app configuration
 *
 * This struct contains all the fields required for operation. The libsoup
 * configuration and WebRTC specific configurations are members here as
 * well.
 *
 */
typedef struct
{
    gchar auth_secret[128];
    gboolean verbose;

    gchar app_version[32];
    gchar signalling_version[32];
    gchar schema_version[32];
    gchar server_url[256];
    gchar server_access_token[1024];
    gchar device_id[64];
    gboolean audio_transmit;
    gboolean audio_receive;
    gint video_stream_id;
    gchar gst_log_level[64];
    gchar adc_partition[32];

    SoupConfig soup;
    WebrtcConfig webrtc;
} AppConfig;

/**
 * @brief returns result enum as string
 * @param result AppConfigResult enum
 * @return string version of result value
 */
extern gchar const* app_config_result_to_string(AppConfigResult result);

/**
 * @brief update app config with command line args
 * @param args
 */
extern void app_config_update(AppConfig* config, CmdArgs const* cmd_args);

/**
 * @brief parse configuration file for application
 * @param config AppConfig object
 * @param config_file_path path to configuration file
 * @return CONFIG_RESULT_OK upon success, otherwise failure
 *
 * Example usage:
 * @code
 *   #include <stdio.h>
 *
 *   #include "app_config.h"
 *
 *   int main()
 *   {
 *       AppConfig config;
 *
 *       AppConfigResult result = app_config_parse(&config, "/tmp/adc_webrtc_streamer.cfg");
 *       if (result == CONFIG_RESULT_OK)
 *           app_config_print(&config);
 *       else
 *           printf("failed to parse config - error: %s\n", app_config_result_to_string(result));
 *
 *       return 0;
 *   }
 *
 */
extern AppConfigResult app_config_parse(AppConfig* config, char const *config_file_path);

/**
 * @brief create json configuration file
 * @param config AppConfig object
 * @param config_file_path
 * @return CONFIG_RESULT_OK upon success, otherwise failure
 */
AppConfigResult app_config_create_file(AppConfig const* config, char const *config_file_path);

/**
 * @brief initialize app_config module
 *
 * purpose of this module is simply to enable gst debugging
 *
 */
extern void app_config_init(void);

/**
 * @brief print config contents
 * @param config AppConfig object
 */
extern void app_config_print(AppConfig const* config);

#ifdef __cplusplus
}
#endif

#endif /* APP_CONFIG_H */
