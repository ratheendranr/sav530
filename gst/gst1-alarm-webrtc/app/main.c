/*************************************************************************************
* @file main.c
*
* main
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include <gst/gst.h>
#include <gst/sdp/sdp.h>
#include <gio/gio.h>

#define GST_USE_UNSTABLE_API
#include <gst/webrtc/webrtc.h>

/* For signalling api */
#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <glib/ghash.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <version.h>

#include "app_config.h"
#include "peer_session.h"
#include "peer_msg.h"
#include "webrtc_gst.h"
#include "stats.h"
#include "logging.h"
#include "cppinterface.h"

#define DEFAULT_STUN_SERVER "stun://alarmiceserver.devicetask.com:3478"
#define DEFAULT_TURN_SERVER "turn://63708287029:adc_turnuser" \
    ":OWpQf1XSFaZvkUy8ogV+5+bcpJw=@alarmiceserver.devicetask.com:3478"
#define RTP_CAPS_VIDEO "application/x-rtp,media=video," \
    "clock-rate=90000,encoding-name=H264"
#define RTP_CAPS_AUDIO "application/x-rtp,media=audio," \
    "clock-rate=8000,encoding-name=PCMU,media=audio"
#define SUPPORTED_SRC_CAPS "video/x-h264;audio/x-raw"
#define SIGNALLING_PROTOCOL_VERSION "2.0.1"
#define WEBRTC_CONFIG_ERROR_FILE "/tmp/webrtcconfigerror"

static GMainLoop *loop;
static GstBus *bus;
static GstElement *main_pipeline;
static GstElement *back_audio_rtpdepay = NULL;
static GstElement *back_audio_sink = NULL;
static GstElement *back_audio_queue = NULL;
static gchar *backward_audio_owner = NULL;
static SoupWebsocketConnection *ws_conn = NULL;
/* command-line args */
static CmdArgs cmd_args = {
    .auth_secret = NULL,
    .server_url = NULL,
    .access_token = NULL,
    .audio_send = -1,
    .audio_recv = -1,
    .video_stream_id = -1,
    .config_file_path = "/adc/config/adc_webrtc_streamer.cfg",
    .reset = 0,
    .verbose = 0
};
static AppConfig const default_app_config = {
    .app_version = APP_VERSION,
    .signalling_version = SIGNALLING_PROTOCOL_VERSION,
    .schema_version = APP_CONFIG_SCHEMA_VERSION,
    .server_url = "wss:\\/\\/alarmss.devicetask.com:443",
    .server_access_token = "jwt_ss",
    .audio_transmit = TRUE,
    .audio_receive = FALSE,
    .video_stream_id = 1,
    .soup = {
        .strict_ssl = FALSE,
        .logging = FALSE
    },
    .webrtc = {
        .ice_transport_policy = "all",
        .bundle_policy = "max-bundle",
        .network_interface = "all"
    },
    .gst_log_level = "GST_DEBUG=2,main:5,app_config:5",
    //.gst_log_level = "GST_DEBUG=2,webrtc*:6",
    .adc_partition = "/adc"
};
static AppConfig app_config;
static gint ss_reconnect_attempts;
static Stats* stats;

GST_DEBUG_CATEGORY_STATIC (application);
#define GST_CAT_DEFAULT application

static GHashTable *peers_hash;

typedef enum {
  NOT_REMOVING,
  REMOVING,
  REMOVED
} peer_state_t;

typedef struct _Peer Peer;
typedef struct _Peer {
  GObject parent_instance;

  PeerSession* session;
  GstElement *vqueue;
  GstElement *aqueue;
  GstElement *webrtcbin;
  GstPad *vteepad;
  GstPad *ateepad;
  peer_state_t video_removing;
  peer_state_t audio_removing;
  peer_state_t backward_audio_removing;
};

#define PEER_TYPE peer_get_type ()
G_DECLARE_FINAL_TYPE (Peer, peer, WEBRTC, PEER, GObject)
G_DEFINE_TYPE (Peer, peer, G_TYPE_OBJECT)

static void
peer_finalize (GObject *gobject)
{
  Peer * peer = (Peer *) gobject;

  GST_CAT_DEBUG (application, "Peer %s finalizing vs:%d as:%d",
      peer_session_get_peer_id(peer->session), g_atomic_int_get (&peer->video_removing),
      g_atomic_int_get (&peer->audio_removing));

  if (backward_audio_owner &&
      !g_strcmp0 (backward_audio_owner, peer_session_get_peer_id(peer->session))) {
    if (back_audio_sink)
      gst_element_set_state (back_audio_sink, GST_STATE_NULL);
    if (back_audio_queue)
      gst_element_set_state (back_audio_queue, GST_STATE_NULL);
    if (back_audio_rtpdepay)
      gst_element_set_state (back_audio_rtpdepay, GST_STATE_NULL);

    if (back_audio_rtpdepay)
      gst_bin_remove (GST_BIN (main_pipeline), back_audio_rtpdepay);
    if (back_audio_queue)
      gst_bin_remove (GST_BIN (main_pipeline), back_audio_queue);
    if (back_audio_sink)
      gst_bin_remove (GST_BIN (main_pipeline), back_audio_sink);

    back_audio_rtpdepay = NULL;
    back_audio_queue = NULL;
    back_audio_sink = NULL;

    g_free (backward_audio_owner);
    backward_audio_owner = NULL;

    GST_CAT_INFO (application, "Backward audio for peer %s removed. "
             "Next new peer will be the backward audio owner.", peer_session_get_peer_id(peer->session));
  }

  if (peer->webrtcbin) {
    gst_element_set_state (peer->webrtcbin, GST_STATE_NULL);
    gst_bin_remove (GST_BIN (main_pipeline), peer->webrtcbin);
    gst_object_unref (peer->webrtcbin);
    peer->webrtcbin = NULL;
  }

  if (peer->vqueue) {
    gst_element_set_state (peer->vqueue, GST_STATE_NULL);
    gst_bin_remove (GST_BIN (main_pipeline), peer->vqueue);
    gst_object_unref (peer->vqueue);
    peer->vqueue = NULL;
  }

  if (peer->aqueue) {
    gst_element_set_state (peer->aqueue, GST_STATE_NULL);
    gst_bin_remove (GST_BIN (main_pipeline), peer->aqueue);
    gst_object_unref (peer->aqueue);
    peer->aqueue = NULL;
  }

  GST_CAT_DEBUG (application, "Peer %s finalized", peer_session_get_peer_id(peer->session));

  if (peer->session)
      peer_session_destroy(&peer->session);

  G_OBJECT_CLASS (peer_parent_class)->finalize (gobject);
}

static void
peer_dispose (GObject *gobject)
{
  GST_CAT_DEBUG (application, "Peer dispose");

  G_OBJECT_CLASS (peer_parent_class)->dispose (gobject);
}

static void
peer_class_init (PeerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = peer_finalize;
  object_class->dispose = peer_dispose;

  GST_CAT_DEBUG (application, "PeerClass init");
}

static void
peer_init (Peer *self)
{
  GST_CAT_DEBUG (application, "Peer init");
}

static GOptionEntry entries[] =
{
  { "auth-secret", 0, 0, G_OPTION_ARG_STRING, &cmd_args.auth_secret,
    "Webrtc auth secret", "AUTH_SECRET"},
  { "server", 0, 0, G_OPTION_ARG_STRING, &cmd_args.server_url,
    "Signalling server to connect to", "URL" },
  { "access-token", 'a', 0, G_OPTION_ARG_STRING, &cmd_args.access_token,
    "Access token for Cloud SS API", "ACCESS_TOKEN"},
  { "audio-send", 0, 0, G_OPTION_ARG_INT, &cmd_args.audio_send,
    "Enable audio, enabled by default", "AUDIO"},
  { "audio-recv", 0, 0, G_OPTION_ARG_INT, &cmd_args.audio_recv,
    "Enable receive audio", "RECEIVE_AUDIO"},
  { "video-stream", 0, 0, G_OPTION_ARG_INT, &cmd_args.video_stream_id,
    "Alarm video stream 0 - s1(default), 1 - s2, 2 - S3", "VIDEO_S"},
  { "config-file", 0, 0, G_OPTION_ARG_STRING, &cmd_args.config_file_path,
    "File path for app configuration file", "APP_CONFIG"},
  { "reset", 'r', 0, G_OPTION_ARG_NONE, &cmd_args.reset,
    "Factory reset configuration", "RESET_CONFIG"},
  { "verbose", 'v', 0, G_OPTION_ARG_INT, &cmd_args.verbose,
    "Verbose logging", NULL},
  { NULL },
};

#ifdef STATIC_BUILD

#define GST_G_IO_MODULE_DECLARE(name) \
extern void G_PASTE(g_io_, G_PASTE(name, _load)) (gpointer data)
#define GST_G_IO_MODULE_LOAD(name) \
G_PASTE(g_io_, G_PASTE(name, _load)) (NULL)

GST_G_IO_MODULE_DECLARE(gnutls);

static void
gst_load_gio_modules (void)
{
  GTlsBackend *backend;
  const gchar *ca_certs;

  GST_G_IO_MODULE_LOAD (gnutls);

  ca_certs = g_getenv ("CA_CERTIFICATES");

  backend = g_tls_backend_get_default ();
  if (backend && ca_certs) {
    GTlsDatabase *db;
    GError *error = NULL;

    db = g_tls_file_database_new (ca_certs, &error);
    if (db) {
      g_tls_backend_set_default_database (backend, db);
      g_object_unref (db);
    } else {
      g_warning ("Failed to create a database from file: %s",
          error ? error->message : "Unknown");
    }
  }
}

static void
init_static_gstreamer(void) {
  /* Loading glib-networking for libsoup ssl support */
  gst_load_gio_modules ();

  {
    GST_PLUGIN_STATIC_DECLARE(udp);
    GST_PLUGIN_STATIC_DECLARE(srtp);
    GST_PLUGIN_STATIC_DECLARE(nice);
    GST_PLUGIN_STATIC_DECLARE(rtpmanager);
    GST_PLUGIN_STATIC_DECLARE(dtls);
    GST_PLUGIN_STATIC_DECLARE(webrtc);
    GST_PLUGIN_STATIC_DECLARE(coreelements);
    GST_PLUGIN_STATIC_DECLARE(videoparsersbad);
    GST_PLUGIN_STATIC_DECLARE(rtp);
    GST_PLUGIN_STATIC_DECLARE(playback);
    GST_PLUGIN_STATIC_DECLARE(alarm);

    GST_PLUGIN_STATIC_REGISTER(udp);
    GST_PLUGIN_STATIC_REGISTER(srtp);
    GST_PLUGIN_STATIC_REGISTER(nice);
    GST_PLUGIN_STATIC_REGISTER(rtpmanager);
    GST_PLUGIN_STATIC_REGISTER(dtls);
    GST_PLUGIN_STATIC_REGISTER(webrtc);
    GST_PLUGIN_STATIC_REGISTER(coreelements);
    GST_PLUGIN_STATIC_REGISTER(videoparsersbad);
    GST_PLUGIN_STATIC_REGISTER(rtp);
    GST_PLUGIN_STATIC_REGISTER(playback);
#ifdef USE_ALARM_PLUGIN
    GST_PLUGIN_STATIC_REGISTER(alarm);
#endif

#ifndef USE_ALARM_PLUGIN
    GST_PLUGIN_STATIC_DECLARE(videotestsrc);
    GST_PLUGIN_STATIC_DECLARE(audiotestsrc);
    GST_PLUGIN_STATIC_DECLARE(mulaw);

    GST_PLUGIN_STATIC_REGISTER(videotestsrc);
    GST_PLUGIN_STATIC_REGISTER(audiotestsrc);
    GST_PLUGIN_STATIC_REGISTER(mulaw);
#endif
  }

  { /* First initialization of dtlsdec element could take up to 6 seconds,
       we precache this element on start of application, otherwise 
       the very first connection could take 7-8 seconds.
    */
    GstElement *element;
    element = gst_element_factory_make ("dtlsdec", NULL);
    gst_object_unref (element);
  }

  {
      guint major, minor, micro, nano;
      gst_version(&major, &minor, &micro, &nano);
      GST_CAT_INFO(application, "GStreamer v%d.%d.%d.%d plugins registered",
          major, minor, micro, nano);
  };
}
#endif /* STATIC_BUILD */

static void
on_negotiation_needed (GstElement * element, const gchar *peer_id);
static void
on_server_closed (SoupWebsocketConnection * conn G_GNUC_UNUSED,
    gpointer user_data G_GNUC_UNUSED);
static void
on_server_message (SoupWebsocketConnection * conn G_GNUC_UNUSED,
    SoupWebsocketDataType type, GBytes * message,
    gpointer user_data G_GNUC_UNUSED);
static void
connect_to_signalling_server (void);
static void
reconnect_to_signalling_server (void);

static gboolean
cleanup_and_quit_loop (const gchar * msg, int state G_GNUC_UNUSED)
{
  if (msg) {
    GST_CAT_ERROR(application, "%s", msg);
  }

  if (ws_conn) {
    if (soup_websocket_connection_get_state (ws_conn) ==
        SOUP_WEBSOCKET_STATE_OPEN)
      /* This will call us again */
      soup_websocket_connection_close (ws_conn, 1000, "");
    else
      g_object_unref (ws_conn);
  }

  if (main_pipeline) {
    gst_element_set_state (GST_ELEMENT (main_pipeline), GST_STATE_NULL);
    gst_object_unref (main_pipeline);
    main_pipeline = NULL;
  }
  gst_bus_remove_signal_watch (bus);
  gst_deinit ();

  if (stats) {
      stats_destroy(&stats);
  }

  if (loop) {
    g_main_loop_quit (loop);
  }

  /* To allow usage as a GSourceFunc */
  return G_SOURCE_REMOVE;
}

static gchar*
get_string_from_json_object (JsonObject * object)
{
  JsonNode *root;
  JsonGenerator *generator;
  gchar *text;

  /* Make it the root node */
  root = json_node_init_object (json_node_alloc (), object);
  generator = json_generator_new ();
  json_generator_set_root (generator, root);
  text = json_generator_to_data (generator, NULL);

  /* Release everything */
  g_object_unref (generator);
  json_node_free (root);
  return text;
}

static void
on_incoming_stream_fakesink (GstElement * webrtc, GstPad * pad, gchar * peer_id)
{
  gchar *tmp;
  GstElement *fakesink;

  if (GST_PAD_DIRECTION (pad) != GST_PAD_SRC)
    return;

  GST_CAT_DEBUG (application, 
      "Incoming stream added, connecting to fakesink: %s",
      gst_caps_to_string(gst_pad_get_current_caps(pad)));

  tmp = g_strdup_printf ("backward_audio_fakesink_%s", peer_id);
  fakesink = gst_element_factory_make ("fakesink", tmp);
  g_free (tmp);
  gst_bin_add (GST_BIN (main_pipeline), fakesink);
  gst_element_link (webrtc, fakesink);
  gst_element_sync_state_with_parent (fakesink);

  GST_CAT_DEBUG (application, "Backward audio fakesink from peer %s started",
      peer_id);
  g_free (peer_id);
}

static void
on_incoming_stream (GstElement * webrtc, GstPad * pad, GstElement * pipe)
{
  GstCaps *caps, *allowed_caps = gst_caps_from_string (RTP_CAPS_AUDIO);

  if (GST_PAD_DIRECTION (pad) != GST_PAD_SRC)
    return;

  caps = gst_pad_get_current_caps(pad);
  GST_CAT_INFO (application, "Incoming stream added, caps: %s",
           gst_caps_to_string(caps));

  if (gst_caps_can_intersect (caps, allowed_caps)) {
    back_audio_rtpdepay = gst_element_factory_make ("rtppcmudepay", "ba_depay");
    g_assert_nonnull (back_audio_rtpdepay);
    back_audio_queue = gst_element_factory_make ("queue", "ba_queue");
    g_assert_nonnull (back_audio_queue);
#ifdef USE_ALARM_PLUGIN
    back_audio_sink = gst_element_factory_make ("alarmaudiosink", "ba_sink");
    g_assert_nonnull (back_audio_sink);
#else
    back_audio_sink = gst_element_factory_make ("autoaudiosink", "ba_sink");
    g_assert_nonnull (back_audio_sink);
#endif
    gst_bin_add_many (GST_BIN (pipe), back_audio_rtpdepay, back_audio_queue,
        back_audio_sink, NULL);

    gst_element_link_many (webrtc, back_audio_rtpdepay, back_audio_queue,
        back_audio_sink, NULL);

    gst_element_sync_state_with_parent (back_audio_rtpdepay);
    gst_element_sync_state_with_parent (back_audio_queue);
    gst_element_sync_state_with_parent (back_audio_sink);

    GST_CAT_INFO (application, "Backward audio from peer %s started",
        backward_audio_owner);
  }

  gst_caps_unref (caps);
  gst_caps_unref (allowed_caps);
}

static gboolean
on_peer_msg_cb (gchar const* text)
{
    GST_CAT_DEBUG (application, "Sending msg to peer");
    soup_websocket_connection_send_text (ws_conn, text);
    return G_SOURCE_REMOVE;
}

static void
generate_peer_id(gchar* peer_id, gsize max_peer_id_len)
{
    g_snprintf(peer_id, max_peer_id_len, "%s_peer_id", app_config.device_id);
}

static void
send_error_message(gchar const* dest_peer_id, gchar const* error)
{
    gchar* text = NULL;
    gchar peer_id[128] = {0};

    generate_peer_id(peer_id, sizeof(peer_id));
    PeerMsg* msg = peer_msg_new(PEER_MSG_TYPE_ERROR, peer_id, dest_peer_id, error);
    if (msg) {
        text = g_strdup(peer_msg_to_json_string(msg));
        peer_msg_destroy(&msg);

        g_idle_add_full (G_PRIORITY_HIGH, (GSourceFunc) on_peer_msg_cb, text,
            g_free);
    } else {
        GST_CAT_ERROR (application, "failed to send error message PeerMsg obj NULL");
    }
}

static void
send_info_message(gchar const* dest_peer_id, gchar const* message)
{
    gchar* text = NULL;
    gchar peer_id[128] = {0};

    generate_peer_id(peer_id, sizeof(peer_id));
    PeerMsg* msg = peer_msg_new(PEER_MSG_TYPE_INFO, peer_id, dest_peer_id, message);
    if (msg) {
        text = g_strdup(peer_msg_to_json_string(msg));
        peer_msg_destroy(&msg);

        g_idle_add_full (G_PRIORITY_HIGH, (GSourceFunc) on_peer_msg_cb, text,
            g_free);
    } else {
        GST_CAT_ERROR (application, "failed to send info message PeerMsg obj NULL");
    }
}

static gboolean
on_ice_candidate_cb (const gchar *text)
{
  GST_CAT_DEBUG (application, "Executing ice candidate send");

  soup_websocket_connection_send_text (ws_conn, text);

  return G_SOURCE_REMOVE;
}

static void
send_ice_candidate_message (GstElement * webrtc G_GNUC_UNUSED, guint mlineindex,
    gchar * candidate, const gchar * peer_id)
{
  gchar *text;
  JsonObject *ice, *msg;

  GST_CAT_INFO (application, "Send Ice for %s: %s sdpMLineIndex %d", 
      peer_id, candidate, mlineindex);
  ice = json_object_new ();
  json_object_set_string_member (ice, "candidate", candidate);
  json_object_set_int_member (ice, "sdpMLineIndex", mlineindex);
  msg = json_object_new ();
  json_object_set_string_member (msg, "to", peer_id);
  json_object_set_object_member (msg, "ice", ice);
  text = get_string_from_json_object (msg);
  json_object_unref (msg);

  /* We have to execute this in main thread, as libsoup is not thread-safe */
  g_idle_add_full (G_PRIORITY_HIGH, (GSourceFunc) on_ice_candidate_cb, text,
      g_free);
}

static GstPadProbeReturn
unlink_video_branch_cb (GstPad * pad, GstPadProbeInfo * info,
    gpointer user_data)
{
  Peer *peer = (Peer *) user_data;
  const gchar *peer_id = peer_session_get_peer_id(peer->session);
  GstElement *tee;
  GstPad *sinkpad;

  GST_CAT_DEBUG (application, "Removing video branch for peer %s state %d",
      peer_id, peer->video_removing);

  if (!g_atomic_int_compare_and_exchange (&peer->video_removing, NOT_REMOVING,
      REMOVING))
    return GST_PAD_PROBE_OK;

  sinkpad = gst_element_get_static_pad (peer->vqueue, "sink");
  g_assert_nonnull (sinkpad);
  gst_pad_unlink (peer->vteepad, sinkpad);
  gst_object_unref (sinkpad);

  tee = gst_bin_get_by_name (GST_BIN (main_pipeline), "vtee");
  g_assert_nonnull (tee);
  gst_element_release_request_pad (tee, peer->vteepad);
  gst_object_unref (peer->vteepad);
  gst_object_unref (tee);

  GST_CAT_DEBUG (application, "Video branch for peer %s removed", peer_id);
  g_atomic_int_compare_and_exchange (&peer->video_removing, REMOVING, REMOVED);

  if (g_atomic_int_get (&peer->audio_removing) == REMOVED) {
    g_hash_table_remove (peers_hash, peer_session_get_peer_id(peer->session));
    g_object_unref (peer);
  }

  return GST_PAD_PROBE_REMOVE;
}

static GstPadProbeReturn
unlink_audio_branch_cb (GstPad * pad, GstPadProbeInfo * info,
    gpointer user_data)
{
  Peer *peer = (Peer *) user_data;
  GstElement *tee;
  GstPad *sinkpad;

  GST_CAT_DEBUG (application, "Removing audio branch for peer %s state %d",
          peer_session_get_peer_id(peer->session), peer->audio_removing);
  if (!g_atomic_int_compare_and_exchange (&peer->audio_removing, NOT_REMOVING,
      REMOVING))
    return GST_PAD_PROBE_OK;

  sinkpad = gst_element_get_static_pad (peer->aqueue, "sink");
  g_assert_nonnull (sinkpad);

  gst_pad_unlink (peer->ateepad, sinkpad);
  gst_object_unref (sinkpad);

  tee = gst_bin_get_by_name (GST_BIN (main_pipeline), "atee");
  g_assert_nonnull (tee);
  gst_element_release_request_pad (tee, peer->ateepad);
  gst_object_unref (peer->ateepad);
  gst_object_unref (tee);
  
  GST_CAT_DEBUG (application, "Audio branch for peer %s removed",
          peer_session_get_peer_id(peer->session));

  g_atomic_int_compare_and_exchange (&peer->audio_removing, REMOVING, REMOVED);

  if (g_atomic_int_get (&peer->video_removing) == REMOVED) {
    g_hash_table_remove (peers_hash, peer_session_get_peer_id(peer->session));
    g_object_unref (peer);
  }

  return GST_PAD_PROBE_REMOVE;
}

static gboolean
unlink_cb (gpointer user_data)
{
  Peer *peer = (Peer *) user_data;

  peer->video_removing = NOT_REMOVING;
  if (!app_config.audio_transmit) {
    /* We need to set audio state to REMOVED if audio 
       is disabled to finalize the peer
    */
    peer->audio_removing = REMOVED;
  }
  gst_pad_add_probe (peer->vteepad, GST_PAD_PROBE_TYPE_IDLE, 
      unlink_video_branch_cb, peer, NULL);

  if (app_config.audio_transmit) {
    peer->audio_removing = NOT_REMOVING;
    gst_pad_add_probe (peer->ateepad, GST_PAD_PROBE_TYPE_IDLE, 
        unlink_audio_branch_cb, peer, NULL);
  } 

  return G_SOURCE_REMOVE;
}

static gboolean
remove_peer_from_pipeline_probed (const gchar * peer_id)
{
  GstElement *f;
  gboolean remove_backward_audio = FALSE;
  Peer *peer = NULL;
  gchar *tmp = NULL;

  peer = g_hash_table_lookup (peers_hash, peer_id);
  GST_CAT_INFO (application, "Peer %s disconnected", peer_session_get_peer_id(peer->session));
  
  if (app_config.audio_receive && backward_audio_owner) {
      GST_CAT_INFO (application, "Peer is backward audio owner: %d", 
          !g_strcmp0 (backward_audio_owner, peer_session_get_peer_id(peer->session)));

    if (!g_strcmp0 (backward_audio_owner, peer_session_get_peer_id(peer->session)))
      remove_backward_audio = TRUE;
  }
  
  tmp = g_strdup_printf ("backward_audio_fakesink_%s", peer_session_get_peer_id(peer->session));
  f = gst_bin_get_by_name (GST_BIN (main_pipeline), tmp);  
  g_free (tmp);
  if (f) {
    gst_bin_remove (GST_BIN (main_pipeline), f);
    gst_element_set_state (f, GST_STATE_NULL);
    gst_object_unref (f);
  }

  /* Adding video and audio probes in main thread */
  unlink_cb (peer);

  return G_SOURCE_REMOVE;
}

static void
add_peer_to_pipeline (PeerSession* session, gboolean offer)
{
  int ret;
  gchar *tmp;
  GstPad *srcpad, *sinkpad;
  GstElement *tee;
#if GST_CHECK_VERSION (1, 17, 0)
  GstWebRTCRTPTransceiver *transceiver;
#endif
  Peer *peer =  g_object_new (PEER_TYPE, NULL);
  peer->session = session;

  tmp = g_strdup_printf ("vqueue-%s", peer_session_get_peer_id(peer->session));
  peer->vqueue = gst_element_factory_make ("queue", tmp);
  /* Leak peer's old buffers to prevent main pipeline dataflow hanging */
  g_object_set (peer->vqueue, "leaky", 2, NULL);

  g_free (tmp);

  tmp = g_strdup_printf ("webrtcbin-%s", peer_session_get_peer_id(peer->session));
  peer->webrtcbin = gst_element_factory_make ("webrtcbin", tmp);
  g_free (tmp);
  g_assert_nonnull (peer->webrtcbin);
  g_object_set (peer->webrtcbin, "turn-server", peer_session_get_ice_turn_server(session), NULL);
  g_object_set (peer->webrtcbin, "stun-server", peer_session_get_ice_stun_server(session), NULL);
  g_object_set (peer->webrtcbin, "bundle-policy", 
          webrtc_gst_bundle_policy_string_to_enum(app_config.webrtc.bundle_policy), NULL);
  g_object_set(peer->webrtcbin, "ice-transport-policy",
          webrtc_gst_ice_transport_policy_string_to_enum(
                  app_config.webrtc.ice_transport_policy), NULL);

  gst_object_ref (peer->webrtcbin);
  gst_object_ref (peer->vqueue);
  gst_bin_add_many (GST_BIN (main_pipeline), peer->vqueue, peer->webrtcbin,
      NULL);

  /* video queue <-> webrtcbin */
  srcpad = gst_element_get_static_pad (peer->vqueue, "src");
  g_assert_nonnull (srcpad);
  sinkpad = gst_element_get_request_pad (peer->webrtcbin, "sink_%u");
  g_assert_nonnull (sinkpad);
  ret = gst_pad_link (srcpad, sinkpad);
  g_assert_cmpint (ret, ==, GST_PAD_LINK_OK);
  gst_object_unref (srcpad);

#if GST_CHECK_VERSION (1, 17, 0)
  //g_object_get (sinkpad, "transceiver", &transceiver, NULL);
  //g_assert_nonnull (transceiver);

  //gst_webrtc_rtp_transceiver_set_direction (transceiver,
  //    GST_WEBRTC_RTP_TRANSCEIVER_DIRECTION_SENDONLY);
  //g_object_unref (transceiver);
#endif
  gst_object_unref (sinkpad);

  /* audio queue <-> webrtcbin */
  if (app_config.audio_transmit) {
    tmp = g_strdup_printf ("aqueue-%s", peer_session_get_peer_id(peer->session));
    peer->aqueue = gst_element_factory_make ("queue", tmp);
    /* Queue will leak old buffers */
    g_object_set (peer->aqueue, "leaky", 2, NULL);
    g_free (tmp);
    
    gst_object_ref (peer->aqueue);
    gst_bin_add (GST_BIN (main_pipeline), peer->aqueue);
    srcpad = gst_element_get_static_pad (peer->aqueue, "src");
    g_assert_nonnull (srcpad);
    sinkpad = gst_element_get_request_pad (peer->webrtcbin, "sink_%u");
    g_assert_nonnull (sinkpad);
    ret = gst_pad_link (srcpad, sinkpad);
    g_assert_cmpint (ret, ==, GST_PAD_LINK_OK);
    gst_object_unref (srcpad);

    if (!app_config.audio_receive) {
#if GST_CHECK_VERSION (1, 17, 0)
      //g_object_get (sinkpad, "transceiver", &transceiver, NULL);
      //g_assert_nonnull (transceiver);

      //gst_webrtc_rtp_transceiver_set_direction (transceiver,
      //    GST_WEBRTC_RTP_TRANSCEIVER_DIRECTION_SENDONLY);
      //g_object_unref (transceiver);
#endif
    }
    gst_object_unref (sinkpad);
  }

  if (offer) {
    g_signal_connect (peer->webrtcbin, "on-negotiation-needed",
        G_CALLBACK (on_negotiation_needed), (gpointer) peer_session_get_peer_id(peer->session));
  }

  GObject* iceagent;
  g_object_get (peer->webrtcbin, "ice-agent", &iceagent, NULL);
  //g_signal_emit_by_name (iceagent, "add-local-ip-address", "192.168.1.144");
  //g_signal_emit_by_name (iceagent, "add-local-ip-address", "192.168.200.11");
  g_signal_connect (peer->webrtcbin, "on-ice-candidate",
      G_CALLBACK (send_ice_candidate_message), (gpointer) peer_session_get_peer_id(peer->session));
  
  /* Set to pipeline branch to PLAYING */
  ret = gst_element_sync_state_with_parent (peer->vqueue);
  g_assert_true (ret);
  if (app_config.audio_transmit) {
    ret = gst_element_sync_state_with_parent (peer->aqueue);
    g_assert_true (ret);
  }
  ret = gst_element_sync_state_with_parent (peer->webrtcbin);
  g_assert_true (ret);

  /* We MUST link new peer's peer branch to the main pipeline ONLY 
     after the states of the peer branch's sinks set to PLAYING,
     otherwise, we will get flushing sink sooner or later.
  */
  /* video tee <-> video queue */
  tee = gst_bin_get_by_name (GST_BIN (main_pipeline), "vtee");
  g_assert_nonnull (tee);
  peer->vteepad = gst_element_get_request_pad (tee, "src_%u");
  g_assert_nonnull (srcpad);
  gst_object_unref (tee);
  sinkpad = gst_element_get_static_pad (peer->vqueue, "sink");
  g_assert_nonnull (sinkpad);
  ret = gst_pad_link (peer->vteepad, sinkpad);
  g_assert_cmpint (ret, ==, GST_PAD_LINK_OK);
  gst_object_unref (sinkpad);

  if (peer->aqueue && app_config.audio_transmit) {
    /* audio tee <-> audio queue */
    tee = gst_bin_get_by_name (GST_BIN (main_pipeline), "atee");
    g_assert_nonnull (tee);
    peer->ateepad = gst_element_get_request_pad (tee, "src_%u");
    g_assert_nonnull (peer->ateepad);
    gst_object_unref (tee);
    sinkpad = gst_element_get_static_pad (peer->aqueue, "sink");
    g_assert_nonnull (sinkpad);
    ret = gst_pad_link (peer->ateepad, sinkpad);
    g_assert_cmpint (ret, ==, GST_PAD_LINK_OK);
    gst_object_unref (sinkpad);
  }

  GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (main_pipeline),
      GST_DEBUG_GRAPH_SHOW_ALL, "peer-added");

  g_hash_table_insert (peers_hash, peer_session_get_peer_id(peer->session), peer);
  GST_CAT_INFO (application, "Pipeline for peer %s added", peer_session_get_peer_id(peer->session));
}

static gboolean
call_peer (PeerSession* session)
{
  if (!g_hash_table_contains (peers_hash, peer_session_get_peer_id(session))) {
    add_peer_to_pipeline(session, TRUE);

    /* Stop timer */
    return G_SOURCE_REMOVE;
  }

  return G_SOURCE_CONTINUE;
}

static gboolean
on_sdp_cb (gchar *sdptext)
{
  soup_websocket_connection_send_text (ws_conn, sdptext);

  return G_SOURCE_REMOVE;
}

static void
send_peer_sdp (GstWebRTCSessionDescription * desc, const gchar * peer_id)
{
  JsonObject *msg, *sdp;
  gchar *text, *sdptext;
  const gchar *sdptype;

  if (desc->type == GST_WEBRTC_SDP_TYPE_OFFER)
    sdptype = "offer";
  else if (desc->type == GST_WEBRTC_SDP_TYPE_ANSWER)
    sdptype = "answer";
  else
    g_assert_not_reached ();

  text = gst_sdp_message_as_text (desc->sdp);
  GST_CAT_INFO (application, "Sending sdp %s to %s", sdptype, peer_id);

  for (int i = 0; i < gst_sdp_message_medias_len (desc->sdp); i++) {
    GstSDPMedia *media = gst_sdp_message_get_media (desc->sdp, i);

    GST_CAT_INFO (application, "Media: %s", gst_sdp_media_get_media (media));

    for (int j = 0; j < gst_sdp_media_attributes_len (media); j++) {
      const GstSDPAttribute *attr = gst_sdp_media_get_attribute (media, j);

      GST_CAT_INFO (application,  "\t attr : [%s:%s]", attr->key, attr->value);
    }
  }

  sdp = json_object_new ();
  json_object_set_string_member (sdp, "type", sdptype);
  json_object_set_string_member (sdp, "sdp", text);
  g_free (text);

  msg = json_object_new ();
  json_object_set_string_member (msg, "to", peer_id);
  json_object_set_object_member (msg, "sdp", sdp);
  sdptext = get_string_from_json_object (msg);
  json_object_unref (msg);

  g_idle_add_full (G_PRIORITY_HIGH, (GSourceFunc) on_sdp_cb, sdptext, g_free);
}

/* Offer created by our pipeline, to be sent to the peer */
static void
on_offer_created (GstPromise * promise, const gchar * peer_id)
{
  GstElement *webrtc;
  GstWebRTCSessionDescription *offer = NULL;
  const GstStructure *reply;
  gchar* tmp = NULL;

  GST_CAT_INFO (application, "Offer created: %s", peer_id);
  g_assert_cmphex (gst_promise_wait(promise), ==, GST_PROMISE_RESULT_REPLIED);
  reply = gst_promise_get_reply (promise);
  gst_structure_get (reply, "offer",
      GST_TYPE_WEBRTC_SESSION_DESCRIPTION, &offer, NULL);
  gst_promise_unref (promise);

  promise = gst_promise_new ();
  GST_CAT_INFO (application, "Lookup element %s", peer_id);
  tmp = g_strdup_printf ("webrtcbin-%s", peer_id);
  webrtc = gst_bin_get_by_name (GST_BIN (main_pipeline), tmp);
  g_free (tmp);
  g_assert_nonnull (webrtc);
  g_signal_emit_by_name (webrtc, "set-local-description", offer, promise);
  gst_promise_interrupt (promise);
  gst_promise_unref (promise);
  g_object_unref (webrtc);

  /* Send offer to peer */
  send_peer_sdp (offer, peer_id);
  gst_webrtc_session_description_free (offer);
}

static void
on_negotiation_needed (GstElement * element, const gchar *peer_id)
{
  GstPromise *promise;

  GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (main_pipeline),
                            GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");

  GST_CAT_INFO (application, "Negotiation required: %s", peer_id);
  promise = gst_promise_new_with_change_func (
      (GstPromiseChangeFunc) on_offer_created, (gpointer) peer_id, NULL);
  g_signal_emit_by_name (element, "create-offer", NULL, promise);
}

static GstBusSyncReply
bus_sync_handler (GstBus * bus  G_GNUC_UNUSED,
                  GstMessage * message  G_GNUC_UNUSED,
                  GstPipeline * data G_GNUC_UNUSED)
{
  return GST_BUS_PASS;
}

static void
state_changed_cb (GstBus * bus  G_GNUC_UNUSED, GstMessage * msg,
                  gpointer inst  G_GNUC_UNUSED)
{
  GstState old_state, new_state, pending_state;
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (main_pipeline)) {
    gst_message_parse_state_changed (msg, &old_state, &new_state,
                                     &pending_state);
    GST_CAT_INFO (application, "State changed to %s",
          gst_element_state_get_name (new_state));
    if (old_state == GST_STATE_PAUSED && new_state == GST_STATE_READY) {
    }
    if (new_state == GST_STATE_PLAYING) {
      GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (main_pipeline),
                                GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");
    }

    if (new_state == GST_STATE_PAUSED && old_state == GST_STATE_PLAYING) {
    }
  }
}

static void
G_GNUC_UNUSED
property_notify_cb (GstBus * bus  G_GNUC_UNUSED, GstMessage * message,
    gpointer inst  G_GNUC_UNUSED)
{
  const GValue *val;
  const gchar *name;
  GstObject *obj;
  gchar *val_str = NULL;
  gchar *obj_name;

  gst_message_parse_property_notify (message, &obj, &name, &val);

  /* Let's not print anything for excluded properties... */
  obj_name = gst_object_get_path_string (GST_OBJECT (obj));
  if (val != NULL) {
    if (G_VALUE_HOLDS_STRING (val))
      val_str = g_value_dup_string (val);
    else if (G_VALUE_TYPE (val) == GST_TYPE_CAPS)
      val_str = gst_caps_to_string (g_value_get_boxed (val));
    else if (G_VALUE_TYPE (val) == GST_TYPE_TAG_LIST)
      val_str = gst_tag_list_to_string (g_value_get_boxed (val));
    else if (G_VALUE_TYPE (val) == GST_TYPE_STRUCTURE)
      val_str = gst_structure_to_string (g_value_get_boxed (val));
    else
      val_str = gst_value_serialize (val);
  } else {
    val_str = g_strdup ("(no value)");
  }

  GST_CAT_INFO (application, "%s: %s = %s", obj_name, name, val_str);
  g_free (obj_name);
  g_free (val_str);
  return;
}

static gboolean
handle_keyboard (GIOChannel *source, GIOCondition cond, gpointer data) {
  gchar *str = NULL;

  if (g_io_channel_read_line (source, &str, NULL, NULL, NULL)
      != G_IO_STATUS_NORMAL) {
    return TRUE;
  }

  switch (g_ascii_tolower (str[0])) {
  case 'd':
    GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (main_pipeline),
        GST_DEBUG_GRAPH_SHOW_ALL, "pipeline-dump-on-request");
    break;
  case 't':
    malloc_trim (0);
    break;
  case 's':
    gst_element_set_state (GST_ELEMENT (main_pipeline), GST_STATE_NULL);
    g_object_unref (main_pipeline);
    main_pipeline = NULL;
    if (ws_conn) {
      g_signal_handlers_disconnect_by_func (ws_conn, 
          G_CALLBACK (on_server_closed), NULL);
      g_signal_handlers_disconnect_by_func (ws_conn, 
          G_CALLBACK (on_server_message), NULL);
      if (soup_websocket_connection_get_state (ws_conn) ==
          SOUP_WEBSOCKET_STATE_OPEN)
        /* This will call us again */
        soup_websocket_connection_close (ws_conn, 1000, "");
      else
        g_object_unref (ws_conn);
    }
    break;
  case 'e':
    exit (0);
  case 'q':
    return cleanup_and_quit_loop ("User stopped pipeline", 0);
    break;
  default:
    break;
  }
  g_free (str);

  return TRUE;
}

static gboolean
G_GNUC_UNUSED
message_cb (GstBus * bus, GstMessage * message, gpointer user_data)
{

  GST_CAT_INFO (application, "Message %d received", GST_MESSAGE_TYPE (message));

  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_ERROR:{
      GError *err = NULL;
      gchar *name, *debug = NULL;

      name = gst_object_get_path_string (message->src);
      gst_message_parse_error (message, &err, &debug);

      GST_CAT_ERROR (application, "ERROR: from element %s: %s", name,
          err->message);
      if (debug != NULL)
        GST_CAT_ERROR (application, "Additional debug info: %s", debug);

      g_error_free (err);
      g_free (debug);
      g_free (name);

      g_main_loop_quit (loop);
      break;
    }
    case GST_MESSAGE_WARNING:{
      GError *err = NULL;
      gchar *name, *debug = NULL;

      name = gst_object_get_path_string (message->src);
      gst_message_parse_warning (message, &err, &debug);

      GST_CAT_WARNING (application, "Warning: from element %s: %s", name,
          err->message);
      if (debug != NULL)
        GST_CAT_WARNING (application, "Additional debug info:%s", debug);

      g_error_free (err);
      g_free (debug);
      g_free (name);
      break;
    }
    case GST_MESSAGE_EOS:
      GST_CAT_WARNING (application, "Got EOS");
      g_main_loop_quit (loop);
      break;
    default:
      break;
  }

  return TRUE;
}

static gboolean
start_pipeline (void)
{
  GstStateChangeReturn ret;
  GError *error = NULL;
  GString *cmd = g_string_new(NULL);

  /* video */
#ifdef USE_ALARM_PLUGIN
  g_string_printf (cmd, "alarmsrc connection=%d ", app_config.video_stream_id);
#else 
  g_string_printf (cmd,
      "videotestsrc is-live=true do-timestamp=true ! video/x-raw,framerate=10/1,width=320,height=240 ! "\
      "x264enc bitrate=500 threads=1 tune=zerolatency speed-preset=ultrafast "\
      "byte-stream=true ! identity sync=true ! "\
      "video/x-h264,profile=constrained-baseline ");
#endif

  cmd = g_string_append (cmd,
      " ! h264parse config-interval=-1 disable-passthrough=true"
      " ! rtph264pay config-interval=-1 ! " RTP_CAPS_VIDEO
      " ! tee name=vtee ! queue ! fakesink ");

#if 0
  cmd = g_string_append (cmd,
      " ! h264parse config-interval=-1 disable-passthrough=true"
      " ! rtph264pay name=pay0 pt=96");
#endif

  /* audio */
  if (app_config.audio_transmit) {
#ifdef USE_ALARM_PLUGIN
    cmd = g_string_append (cmd, " alarmsrc connection=3 ");
#else
    cmd = g_string_append (cmd, " audiotestsrc is-live=true do-timestamp=true ! mulawenc ");
#endif
    cmd = g_string_append (cmd,
      " ! rtppcmupay ! " RTP_CAPS_AUDIO
      " ! tee name=atee ! queue ! fakesink ");
  }

  main_pipeline =
      gst_parse_launch (cmd->str , &error);
  g_string_free (cmd, TRUE);

  if (error) {
    GST_CAT_ERROR (application, "Failed to parse launch: %s", error->message);
    g_error_free (error);
    goto err;
  }

  gst_element_add_property_deep_notify_watch (main_pipeline, NULL, TRUE);
  bus = gst_element_get_bus (main_pipeline);
  gst_bus_set_sync_handler (bus, (GstBusSyncHandler) bus_sync_handler, NULL,
                           NULL);
  gst_bus_add_signal_watch_full (bus, G_PRIORITY_HIGH);
  gst_bus_enable_sync_message_emission (bus);

  g_signal_connect (G_OBJECT (bus), "message::state-changed",
      (GCallback) state_changed_cb, NULL);
  g_object_unref (bus);

  GST_CAT_INFO (application, "Starting pipeline");
  ret = gst_element_set_state ( GST_ELEMENT (main_pipeline), GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE)
    goto err;

  return TRUE;
err:
  if (main_pipeline)
    g_clear_object (&main_pipeline);
  return FALSE;
}

static gboolean
register_with_server (void)
{
  gchar *hello;

  if (soup_websocket_connection_get_state (ws_conn) !=
      SOUP_WEBSOCKET_STATE_OPEN)
    return FALSE;

  GST_CAT_INFO (application, "Registering with signalling protocol version %s",
      SIGNALLING_PROTOCOL_VERSION);

  /* Register with the server with a signalling protocol version
   * Reply will be received by on_server_message()
   */
  hello = g_strdup_printf ("HELLO %s", SIGNALLING_PROTOCOL_VERSION);
  soup_websocket_connection_send_text (ws_conn, hello);
  g_free (hello);

  return TRUE;
}

static void
on_server_closed (SoupWebsocketConnection * conn G_GNUC_UNUSED,
    gpointer user_data G_GNUC_UNUSED)
{
  /* Reconnect to signalling server */
  GST_CAT_ERROR (application,
      "Signalling server closed connection, reconnecting...");

  stats_increment_ss_disconnects(stats);

  if (ws_conn) {
    if (soup_websocket_connection_get_state (ws_conn) ==
        SOUP_WEBSOCKET_STATE_OPEN)
      /* This will call us again */
      soup_websocket_connection_close (ws_conn, 1000, "");
    else
      g_object_unref (ws_conn);
  }

  reconnect_to_signalling_server();
}

/* One mega message handler for our asynchronous calling mechanism */
static void
on_server_message (SoupWebsocketConnection * conn G_GNUC_UNUSED,
    SoupWebsocketDataType type, GBytes * message,
    gpointer user_data G_GNUC_UNUSED)
{
  gsize size;
  gchar *text; 
  const gchar *data;

  switch (type) {
    case SOUP_WEBSOCKET_DATA_BINARY:
      GST_CAT_ERROR (application, "Received unknown binary message, ignoring");
      return;
    case SOUP_WEBSOCKET_DATA_TEXT:
      data = g_bytes_get_data (message, &size);
      /* Convert to NULL-terminated string */
      text = g_strndup (data, size);
      break;
    default:
      g_assert_not_reached ();
  }

  /* Server has accepted our registration, we are ready to send commands */
  if (g_str_has_prefix(text, "HELLO")) {
    GST_CAT_INFO (application, "Registered with server");
    gchar ** text_strings = g_strsplit(text, " ", -1);
    if (g_strv_length (text_strings) < 2) {
      GST_CAT_ERROR(application, "SS node DNS: no DNS string detected");
      goto out;
    }
    gchar * ss_node_dns = text_strings[1];
    GST_CAT_INFO(application, "SS node DNS: %s", ss_node_dns);
    g_autoptr(GString) cmd = g_string_new(NULL);
    g_string_printf(cmd, "%s/adcclientapp --ss_node %s", app_config.adc_partition, ss_node_dns);
    system(cmd->str);
    g_strfreev(text_strings);
    ss_reconnect_attempts = 0;
  } else if (g_str_has_prefix(text, "START_SESSION")) {

    GST_CAT_INFO (application, "%s", strstr(text, "{"));

    PeerSession* session = peer_session_new();
    if (session) {
        PeerSessionResult result = peer_session_parse(session, strstr(text, "{"));
        if (result != PEER_SESSION_RESULT_OK) {
            GST_CAT_ERROR(application, "%s", peer_session_result_to_string(result));
            peer_session_destroy(&session);
            goto out;
        }

        if (!peer_session_valid_auth(session, (guchar*)app_config.auth_secret)) {
            GST_CAT_ERROR(application, "Invalid auth token");
            stats_increment_auth_failures(stats);
            /* notify backend via adcclient */

            peer_session_destroy(&session);
            goto out;
        }
    } else {
        GST_CAT_ERROR(application, "Could not create PeerSession obj");
        goto out;
    }

    stats_increment_total_sessions(stats);

    start_pipeline();

    /* FIXME: We start the peer pipeline in two ways depending on
              was the peer with same name already stopped before we
              here or not. Possibly peer 
    */
    if (g_hash_table_contains (peers_hash, peer_session_get_peer_id(session))) {
      GST_CAT_WARNING (application, "Peer %s is not stopped yet, waiting",
              peer_session_get_peer_id(session));
      g_timeout_add_seconds (1, (GSourceFunc) call_peer, (gpointer) session);
    } else {
      call_peer (session);
    }
  } else if (g_str_has_prefix (text, "SESSION_STOPPED")) {
    gchar *peer_id = g_strdup (strstr (text, " ") + 1);

    if (g_hash_table_contains (peers_hash, peer_id))
      remove_peer_from_pipeline_probed (peer_id);
    else
      g_warn_if_reached ();

    gst_element_set_state (main_pipeline, GST_STATE_NULL);
    g_object_unref (main_pipeline);
    main_pipeline = NULL;
  } else if (g_str_has_prefix (text, "ERROR")) {
    cleanup_and_quit_loop (text, 0);
  /* Look for JSON messages containing SDP and ICE candidates */
  } else {
    JsonNode *root;
    JsonObject *object, *child;
    JsonParser *parser = json_parser_new ();

    GST_CAT_INFO (application, "%s", text);

    if (!json_parser_load_from_data (parser, text, -1, NULL)) {
      GST_CAT_ERROR (application, "Unknown message '%s', ignoring", text);
      g_object_unref (parser);
      goto out;
    }

    root = json_parser_get_root (parser);
    if (!JSON_NODE_HOLDS_OBJECT (root)) {
      GST_CAT_ERROR (application, "Unknown json message '%s', ignoring", text);
      g_object_unref (parser);
      goto out;
    }

    object = json_node_get_object (root);
    /* Check type of JSON message */
    if (json_object_has_member (object, "sdp")) {
      int ret;
      GstSDPMessage *sdp;
      const gchar *text, *peer;
      GstWebRTCSessionDescription *answer;
      const gchar* sdptype = NULL;

      child = json_object_get_object_member (object, "sdp");

      if (!json_object_has_member (child, "type")) {
        cleanup_and_quit_loop ("ERROR: received SDP without 'type'",
            0);
        goto out;
      }

      sdptype = json_object_get_string_member (child, "type");
      GST_CAT_INFO (application, "sdp type: %s", sdptype);

      text = json_object_get_string_member (child, "sdp");
      peer = json_object_get_string_member (object, "from");

      GST_CAT_INFO (application, "Received answer %s:%s", peer, text);

      ret = gst_sdp_message_new (&sdp);
      g_assert_cmphex (ret, ==, GST_SDP_OK);

      ret = gst_sdp_message_parse_buffer ((guint8 *) text, strlen (text), sdp);
      g_assert_cmphex (ret, ==, GST_SDP_OK);

      answer = gst_webrtc_session_description_new (GST_WEBRTC_SDP_TYPE_ANSWER,
          sdp);
      g_assert_nonnull (answer);

      { /* 
          1. Set remote description on our pipeline.
          2. Connect backward audio signal callback if remote
             peer answered with send capability for audio.
        */
        gboolean has_backward_audio = FALSE;
        
        for (int i = 0; i < gst_sdp_message_medias_len(sdp); i++) {
          const GstSDPMedia* media = gst_sdp_message_get_media (sdp, i);
          GST_CAT_INFO (application, "Media: %s", gst_sdp_media_get_media (media));

          for (int j = 0; j < gst_sdp_media_attributes_len (media); j++) {
            const GstSDPAttribute *attr =
                gst_sdp_media_get_attribute (media, j);

            if (!g_strcmp0(gst_sdp_media_get_media (media), "audio") &&
                (!g_strcmp0(attr->key, "sendrecv") || !g_strcmp0(attr->key,
                    "sendonly")))
              has_backward_audio = TRUE;

            GST_CAT_INFO (application,  "\t attr : [%s:%s]", attr->key, attr->value);
          }
        }

        {
          GstPromise *promise = gst_promise_new ();
          gchar *tmp = g_strdup_printf ("webrtcbin-%s", peer);
          GstElement *webrtc = gst_bin_get_by_name (GST_BIN (main_pipeline),
              tmp);
          g_free (tmp);
          g_signal_emit_by_name (webrtc, "set-remote-description", answer,
              promise);
          gst_promise_interrupt (promise);
          gst_promise_unref (promise);

          if (has_backward_audio) {
            GST_CAT_INFO (application, "Backward audio %s, owner peer %s", 
                app_config.audio_receive ? "enabled" : "disabled",
                backward_audio_owner);

            GST_CAT_INFO (application, 
                "Peer %s can send backward audio to us", peer);

            if (app_config.audio_receive && !backward_audio_owner) {
              backward_audio_owner = g_strdup (peer);
              GST_CAT_INFO (application,
                  "Peer %s is backward audio owner now", peer);           
              /* Incoming streams will be exposed via this signal */
              g_signal_connect (webrtc, "pad-added",
                  G_CALLBACK (on_incoming_stream), main_pipeline);
            } else {
              GST_CAT_INFO (application,
                  "Backward audio channel is busy by peer %s",
                  backward_audio_owner);
              g_signal_connect (webrtc, "pad-added",
                  G_CALLBACK (on_incoming_stream_fakesink), g_strdup (peer));
            } 
          }
          g_object_unref (webrtc);
        }
      }
    } else if (json_object_has_member (object, "ice")) {
      const gchar *candidate, *peer;
      gint sdpmlineindex;
      GstElement *webrtc;
      gchar *tmp;

      child = json_object_get_object_member (object, "ice");
      candidate = json_object_get_string_member (child, "candidate");
      sdpmlineindex = json_object_get_int_member (child, "sdpMLineIndex");
      peer = json_object_get_string_member (object, "from");

      GST_CAT_INFO (application, "New remote ICE candidate from %s", peer);
      if (strlen(candidate)) {
        /* Add ice candidate sent by remote peer */
        tmp = g_strdup_printf ("webrtcbin-%s", peer);
        webrtc = gst_bin_get_by_name (GST_BIN (main_pipeline), tmp);
        g_free (tmp);
        g_signal_emit_by_name (webrtc, "add-ice-candidate", sdpmlineindex,
            candidate);
        g_object_unref (webrtc);
      }
    } else {
      GST_CAT_ERROR (application, "Ignoring unknown JSON message:%s", text);
    }
    g_object_unref (parser);
  }

out:
  g_free (text);
}

static void
on_server_connected (SoupSession * session, GAsyncResult * res,
    SoupMessage *msg)
{
  GError *error = NULL;

  ws_conn = soup_session_websocket_connect_finish (session, res, &error);
  if (error) {
    cleanup_and_quit_loop (error->message, 0);
    g_error_free (error);
    return;
  }

  g_assert_nonnull (ws_conn);

  GST_CAT_INFO (application, "Connected to signalling server");

  stats_increment_ss_connects(stats);

  g_signal_connect (ws_conn, "closed", G_CALLBACK (on_server_closed), NULL);
  g_signal_connect (ws_conn, "message", G_CALLBACK (on_server_message), NULL);

  /* Register with the server so it knows about us and can accept commands */
  register_with_server ();
}

/*
 * Signalling server connection.
 */
static void connect_to_signalling_server(void)
{
    SoupLogger *logger;
    SoupMessage *message;
    SoupSession *session;
    const char *https_aliases[] = { "wss", NULL };
    char *auth_header = NULL;

    session = soup_session_new_with_options(SOUP_SESSION_SSL_STRICT, app_config.soup.strict_ssl,
            SOUP_SESSION_SSL_USE_SYSTEM_CA_FILE, TRUE,
            SOUP_SESSION_HTTP_ALIASES, https_aliases, NULL);

    g_assert_nonnull(session);

    if (app_config.verbose || app_config.soup.logging) {
        logger = soup_logger_new(SOUP_LOGGER_LOG_BODY, -1);
        soup_session_add_feature(session, SOUP_SESSION_FEATURE(logger));
        g_object_unref(logger);
    }

    message = soup_message_new(SOUP_METHOD_GET, app_config.server_url);
    if (!message) {
        GST_CAT_ERROR(application, "failed to parse server uri");
        exit(0);
    }

    auth_header = g_strdup_printf("Acc %s", app_config.server_access_token);
    soup_message_headers_append(message->request_headers, "Authorization",
            auth_header);

    GST_CAT_INFO(application, "Connecting to %s...", app_config.server_url);

    /* Once connected, we will register */
    soup_session_websocket_connect_async(session, message, NULL, NULL, NULL,
            (GAsyncReadyCallback) on_server_connected, message);

    g_object_unref(session);
    g_object_unref(message);
}

static void reconnect_to_signalling_server(void)
{
    /* TODO - put some delay between attempts */
    if (++ss_reconnect_attempts < 5) {
        connect_to_signalling_server();
    } else {
        cleanup_and_quit_loop("failed to connect to server, giving up", 0);
        exit(0);
    }
}

static gboolean
check_plugins (void)
{
  int i;
  gboolean ret;
  GstPlugin *plugin;
  GstRegistry *registry;
  const gchar *needed[] = { "nice", "webrtc", "dtls", "srtp",
      "rtpmanager", NULL};

  registry = gst_registry_get ();
  ret = TRUE;
  for (i = 0; i < g_strv_length ((gchar **) needed); i++) {
    plugin = gst_registry_find_plugin (registry, needed[i]);
    if (!plugin) {
      GST_CAT_ERROR (application, "Required gstreamer plugin '%s' not found",
          needed[i]);
      ret = FALSE;
      continue;
    }
    gst_object_unref (plugin);
  }
  return ret;
}

static gboolean
check_args (void)
{
  if (cmd_args.video_stream_id < -1 || cmd_args.video_stream_id > 2) {
    GST_CAT_ERROR (application, "invalid value %d for option --video-stream",
        cmd_args.video_stream_id);
    return FALSE;
  }
  return TRUE;
}

/*
  This will prevent the heap growth, the app doesn't have memory leaks
  but due to big amount of allocations(especially on client start) we suffer
  from the memory fragmentation.
  GLIBC by default has 8 memory pools per CPU core and it keeps wasting memory
  of all these pools without freeing it back to kernel so the RSS of the process
  grows just the same way as if we had a memory leak.
  We set max pools number to 2 and this stops RSS growth.
*/
static void
configure_heap_allocator (void)
{
  mallopt (M_ARENA_MAX, 1);
}


static void
init_modules(void)
{
    app_config_init();
}

static void
create_config_error_file(void)
{
    system("touch " WEBRTC_CONFIG_ERROR_FILE);
}

int
main (int argc, char *argv[])
{
  GOptionContext *context;
  GError *error = NULL;
  GIOChannel *io_stdin;

  /* set default log level */
  if (putenv((char*)default_app_config.gst_log_level))
    g_print("error setting gst log level\n");

  g_print("======================================================\n");
  g_print("Alarm.com WebRTC Streamer v%d.%d.%d %s %s\n", APP_VERSION_MAJOR,
          APP_VERSION_MINOR, APP_VERSION_PATCH, __DATE__, __TIME__);
  g_print("======================================================\n");

  context = g_option_context_new ("- Alarm.com WebRTC Streamer");
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_add_group (context, gst_init_get_option_group ());
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_print("error initializing: %s\n", error->message);
    return -1;
  }

  if (cmd_args.reset) {
      g_print("webrtc factory reset\n");
      g_unlink(cmd_args.config_file_path);
      sync();
      AppConfigResult result = app_config_create_file(&default_app_config, cmd_args.config_file_path);
      if (result != CONFIG_RESULT_OK) {
          g_print("unable to generate default config file error: %s\n", app_config_result_to_string(result));
          /* notify backend */
      }
      sync();
      return 0;
  }

  configure_heap_allocator ();

  if (!check_args ()) {
    return -1;
  }

  /* read config parameters from file */
  AppConfigResult result = app_config_parse(&app_config, cmd_args.config_file_path);
  if (result != CONFIG_RESULT_OK) {
      g_print("unable to parse configuration file error: %s\n", app_config_result_to_string(result));
      g_print("generating new config file...\n");
      /* notify backend */
      /* generate error file that adcclient can pickup */
      create_config_error_file();

      result = app_config_create_file(&default_app_config, cmd_args.config_file_path);
      if (result != CONFIG_RESULT_OK) {
          g_print("unable to generate default config file error: %s\n", app_config_result_to_string(result));
          /* generate error file that adcclient can pickup */
          create_config_error_file();
          return -1;
      }
      result = app_config_parse(&app_config, cmd_args.config_file_path);
      if (result != CONFIG_RESULT_OK) {
          g_print("unable to parse newly generated file error: %s\n", app_config_result_to_string(result));
          /* generate error file that adcclient can pickup */
          create_config_error_file();
          return -1;
      }
  }

  /* override app_config with any specified cmd_args */
  app_config_update(&app_config, &cmd_args);

  /* set log level from config */
  if (putenv((char*)app_config.gst_log_level)) {
      g_print("unable to update log level from config\n");
  }

  /* Glib logging */
  gst_init(NULL, NULL);
  GST_DEBUG_CATEGORY_INIT (application, "main", 0,
      "Alarm.com WebRTC streaming app");
  GST_CAT_INFO(application, "Alarm.com WebRTC Streamer v%d.%d.%d %s %s", APP_VERSION_MAJOR,
          APP_VERSION_MINOR, APP_VERSION_PATCH, __DATE__, __TIME__);

#ifdef STATIC_BUILD
  init_static_gstreamer ();
#endif

  init_modules();

  gst_debug_add_log_function(klog_gst_debug_log, NULL, NULL);

  app_config_print(&app_config);

  if (!check_plugins ()) {
    return -1;
  }
  io_stdin = g_io_channel_unix_new (fileno (stdin));
  g_io_add_watch (io_stdin, G_IO_IN, (GIOFunc)handle_keyboard, NULL);

  loop = g_main_loop_new (NULL, FALSE);

  stats = stats_new(g_main_loop_get_context(loop), "/tmp/adc-webrtc-stats", 60);

  /* Peer objects hash map */
  peers_hash = g_hash_table_new (g_str_hash, g_str_equal);

  //connect_to_signalling_server ();

  //start_pipeline ();
  
  if (argc < 4) {
    printf ("Invalid arguments... usage: <executable> port username password\n");
    return -1;
  }
  /* rtsp port, username, password */
  initializertspserver(argv[1], argv[2], argv[3]);
  g_main_loop_run (loop);

  return 0;
}
