#ifndef SERVER_H_
#define SERVER_H_

#include <iostream>
#include <gst/rtsp-server/rtsp-server.h>
#include <memory>
#include <string>

enum class SourceType { UNDEFINED_SOURCE, FILE_SOURCE, RTSP_SOURCE, VIDEOTESTSRC_SOURCE, DEVICE_SOURCE};
class RtspServer;

typedef struct ServerConfig {
  ServerConfig() : digest(false), source_type(SourceType::UNDEFINED_SOURCE), time(false), 
    pipeline("") {}
  std::string usrname;
  std::string pass;
  std::string route;
  std::string address;
  std::string port;
  enum SourceType source_type;
  std::string input;
  std::string framerate;
  std::pair<std::string, std::string> scale;
  bool time;
  bool digest;
  std::string pipeline;
} ServerConfig;

typedef struct ServerCtx {

  ServerCtx() : loop(nullptr), server(nullptr), mounts(nullptr), factory(nullptr), auth(nullptr),
    token(nullptr), basic(nullptr) {}
  GMainLoop* loop;
  GstRTSPServer* server;
  GstRTSPMountPoints* mounts;
  GstRTSPMediaFactory* factory;
  GstRTSPAuth* auth;
  GstRTSPToken* token;
  gchar* basic;
  std::shared_ptr<ServerConfig> config;
} ServerCtx;

class RtspServer {
  RtspServer() : loop(nullptr) {};
  ~RtspServer() {};

  static RtspServer *instance_;
  bool Initialize(char *port, char *username, char *password);
  void SetDefaults();

  GMainLoop* loop;
  ServerCtx serverctx;

public:
  static RtspServer *GetInstance(char *port, char *username, char *password) {
    if (!instance_) {
      instance_ = new RtspServer();
      instance_->SetDefaults();
      instance_->Initialize(port, username, password);
    }
    return instance_;
  }
};

#endif /* SERVER_H_ */
