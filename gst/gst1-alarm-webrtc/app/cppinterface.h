#ifndef __CPPINTERFACE_H__
#define __CPPINTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif

void initializertspserver(char *port, char *username, char *password);

#ifdef __cplusplus
}
#endif

#endif /* __CPPINTERFACE_H__ */
