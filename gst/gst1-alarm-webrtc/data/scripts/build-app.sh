#!/bin/bash
###############################################################################
# @file build-app.sh
#
# bash script to manage build of alarm-webrtc-streamer
#
# @author David Park <dpark@alarm.com>
# @date 2020-01-02
#
###############################################################################
#
# Alarm.com Copyright 2000-2021
#
# COPYRIGHT STATEMENT.
# This software has been provided pursuant to a License Agreement containing
# restrictions on its use.  The software contains confidential information,
# trade secrets, patents (pending or approved) and proprietary information of
# Alarm.com and is protected by Federal copyright law.  This
# confidential information or source code files, should not be copied or distributed,
# in any form or medium, disclosed to any third party, or taken outside
# Alarm.com or authorized offices or used in any manner, except with prior
# written agreement with Alarm.com . All rights reserved.
#
# Alarm.com
#
###############################################################################
#set -e
#set -u
#set -o pipefail


/home/pixc/adc-buildroot/buildroot/output/host/bin/meson --cross-file data/cross-file/adc-model-x23.txt --prefix=$(pwd)/install build --strip
#ninja -C build/ install
