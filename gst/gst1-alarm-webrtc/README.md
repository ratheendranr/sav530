README
======

## Description
`alarm-webrtc-streamer` is the WebRTC streaming client app for Alarm.com IP cameras. 
It utilizes GStreamer as the underlying media and connectivity framework.
Custom GStreamer plugins are used to interface with platform specific requirements for handling video and audio.

The `alarm-webrtc-streamer` client runs as a daemon, listening on a wss connection to the backend for any new
streaming session requests.

## System Requirements
* python3 >= 3.5

## Currently Supported Camera Models
* x23
* V622

## Standalone Version
A streamer binary can be made for Linux systems without the need for a camera, as this uses simulated video stream.
To create this binary, use 'x86_64' for the model -m argument, as shown below.

## Building The App
The `build-app.sh` script is responsible for compiling the client, and also provides helper functionality to manage
support for the client. It resides in the `alarm-webrtc-streamer/app/data/scripts` directory, and must be run from 
the `alarm-webrtc-streamer/app` directory.

To display usage information:
    
    ./data/scripts/build-app.sh -h
    
To build `alarm-webrtc-streamer` for a particular model the `-m [MODEL]` should be specified. However, if not 
specified the build will default to the x23 model. For instance, this builds the app for the V622 model:

    ./data/scripts/build-app.sh -m V622

To first do a complete clean, then a build, specify the `-c` flag like the following:

    ./data/scripts/build-app.sh -c -m x23
    
The script also supports the ability to upgrade the toolchain and gst-sdk for the target platform, in the event
where the toolchain for the target has been updated, or changes have been made to gst-sdk.

For example, to upgrade the installed toolchain for the x23 model, use the following command:

    ./data/scripts/build-app.sh -m x23 -u toolchain

Note: if the `-u` upgrade flag is specified, the script only upgrades the desired item then terminates. The
app needs to be rebuilt as shown with the commands above.

* Note: sudo may need to be used before the build script when attempting to obtain and extract the toolchain files
to /opt/toolchains/webrtc folder. For example: sudo ./data/scripts/build-app.sh -c -m x23

For x86_64 environment variables may need to be configured:

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/toolchains/webrtc/x86_64/gst-sdk/lib/gstreamer-1.0:/opt/toolchains/webrtc/x86_64/gst-sdk/lib/gio/modules/:/opt/toolchains/webrtc/x86_64/lib:/opt/toolchains/webrtc/x86_64/gst-sdk/lib

export GST_PLUGIN_PATH=/opt/toolchains/webrtc/x86_64/gst-sdk/lib/gstreamer-1.0