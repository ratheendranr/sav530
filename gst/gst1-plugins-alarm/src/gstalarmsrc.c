/*
 * Copyright ©
 */

#include <gst/gst.h>
#include <gst/check/gsttestclock.h>
#include "gstalarmsrc.h"
#include <stdio.h>

//#define MOCK 1

GST_DEBUG_CATEGORY_STATIC (alarm_src_debug);
#define GST_CAT_DEFAULT alarm_src_debug
#define DEFAULT_CONNECTION GST_ALARM_IPC_CONNECTION_VIDEO_S1

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_CONNECTION,
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

#define parent_class gst_alarm_src_parent_class
G_DEFINE_TYPE (GstAlarmSrc, gst_alarm_src, GST_TYPE_PUSH_SRC);

static void gst_alarm_src_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_alarm_src_get_property (GObject * object, guint prop_id, GValue * value, GParamSpec * pspec);
static GstStateChangeReturn gst_alarm_src_change_state (GstElement * element, GstStateChange transition);
void gst_alarm_src_finalize (GObject * object);

/* GstBaseSrc vmethods */
static gboolean gst_alarm_src_start (GstAlarmSrc *src);
static gboolean gst_alarm_src_stop (GstAlarmSrc *src);
static gboolean gst_alarm_src_unlock (GstBaseSrc *src);
static gboolean gst_alarm_src_unlock_stop (GstBaseSrc *src);

/* GstPushSrc vmethods */
static GstFlowReturn gst_alarm_src_create (GstPushSrc *src, GstBuffer **buf);

/* GObject vmethod implementations */

/* initialize the alarmsrc's class */
static void gst_alarm_src_class_init (GstAlarmSrcClass * klass)
{
    GObjectClass *gobject_class;
    GstElementClass *gstelement_class;
    GstBaseSrcClass *gstbasesrc_class;
    GstPushSrcClass *gstpushsrc_class;

    gobject_class = (GObjectClass *) klass;
    gstelement_class = (GstElementClass *) klass;
    gstbasesrc_class = (GstBaseSrcClass *) klass;
    gstpushsrc_class = (GstPushSrcClass *) klass;

    gstelement_class->change_state = GST_DEBUG_FUNCPTR (gst_alarm_src_change_state);

    gobject_class->set_property = gst_alarm_src_set_property;
    gobject_class->get_property = gst_alarm_src_get_property;
    gobject_class->finalize = gst_alarm_src_finalize;
    gstbasesrc_class->unlock = GST_DEBUG_FUNCPTR (gst_alarm_src_unlock);
    gstbasesrc_class->unlock_stop = GST_DEBUG_FUNCPTR (gst_alarm_src_unlock_stop);
    gstpushsrc_class->create = gst_alarm_src_create;

    g_object_class_install_property (gobject_class, PROP_CONNECTION,
        g_param_spec_enum ("connection", "IPC connection",
            "IPC connection",
            GST_TYPE_ALARM_IPC_CONNECTION, DEFAULT_CONNECTION,
            (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
                G_PARAM_CONSTRUCT)));

    gst_element_class_set_static_metadata (gstelement_class, "Alarm Video/Audio Source",
        "Source/Hardware", "Alarm Video/Audio Source",
        "Author");

    gst_element_class_add_pad_template (gstelement_class, gst_static_pad_template_get (&src_factory));

    GST_DEBUG_CATEGORY_INIT (alarm_src_debug, "alarmsrc", 0, "Alarm IPC source element");

    GstAlarmSrc* self = GST_ALARM_SRC_CAST (gobject_class);
    self->initialized = FALSE;
    GST_DEBUG_OBJECT(self, "Src class init done");
}

static void gst_alarm_src_init (GstAlarmSrc * self)
{
    printf("!!! gst_alarm_src_init, connection %d\n", self->connection);

    gst_base_src_set_live (GST_BASE_SRC (self), TRUE);
    gst_base_src_set_do_timestamp (GST_BASE_SRC (self), TRUE);
    gst_base_src_set_format (GST_BASE_SRC (self), GST_FORMAT_TIME);
    gst_base_src_set_dynamic_size (GST_BASE_SRC (self), TRUE);

    #ifdef MOCK
    int ret = init_mock();
    if (ret)
      exit(1);
    #else
      //self->src_ptr = init_video_plugin();
    #endif

    /* Set base time on first frame from IPC */
    self->last_fp_seqnum = GST_SEQNUM_INVALID;
    self->internal_base_time = GST_CLOCK_TIME_NONE;
}

void gst_alarm_src_finalize (GObject * object)
{ 
    GstAlarmSrc *self = GST_ALARM_SRC_CAST (object);
    printf("!!! gst_alarm_src_finalize, disconnect %d\n", self->connection);

    switch (self->connection) {
        case GST_ALARM_IPC_CONNECTION_VIDEO_S1:
        {
          if (TRUE == self->initialized) {
            close_video_stream(self->src_ptr, (int) self->connection);
            self->initialized = FALSE;
          }
          break;
        }
        case GST_ALARM_IPC_CONNECTION_VIDEO_S2:
        {
          if (TRUE == self->initialized) {
            self->initialized = FALSE;
          }
          break;
        }
        case GST_ALARM_IPC_CONNECTION_VIDEO_S3:
        {   
            break;
        }
        case GST_ALARM_IPC_CONNECTION_AUDIO_IN:
        {
          printf("Stoping audio capture\n\n");
          if (TRUE == self->initialized) {
            stop_audio_capture(self->src_ptr_a);
            g_usleep(1000);
            deinit_audio_plugin(self->src_ptr_a);
            gst_buffer_list_unref(self->list);
            self->initialized = FALSE;
          }
          break;
        }
        case GST_ALARM_IPC_CONNECTION_AUDIO_OUT:
        {   
            break;
        }
        case GST_ALARM_IPC_CONNECTION_EVENT:
        {   
            break;
        }
    }

    G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void gst_alarm_src_set_property (GObject * object, guint prop_id, const GValue * value, GParamSpec * pspec)
{
    GstAlarmSrc *self = GST_ALARM_SRC (object);
    printf("!!! gst_alarm_src_set_property, connection %d\n", self->connection);
    switch (prop_id) {
        case PROP_CONNECTION:
            self->connection = (GstAlarmIPCConnectionEnum) g_value_get_enum (value);
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void gst_alarm_src_get_property (GObject * object, guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstAlarmSrc *self = GST_ALARM_SRC (object);
    switch (prop_id) {
        case PROP_CONNECTION:
            g_value_set_enum (value, self->connection);
        break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static gboolean gst_alarm_src_start (GstAlarmSrc *src)
{
  return TRUE;
}

static gboolean gst_alarm_src_stop (GstAlarmSrc *src)
{
  return TRUE;
}

static gboolean gst_alarm_src_unlock (GstBaseSrc * bsrc)
{
  return TRUE;
}

static gboolean gst_alarm_src_unlock_stop (GstBaseSrc * bsrc)
{
  return TRUE;
}

static GstFlowReturn gst_alarm_src_create (GstPushSrc *src, GstBuffer **buffer)
{ 
    GstMapInfo info;
    fp_frm_t fp;
    GstClockTime timestamp;
    void *frame;
    int ret = 0;
    GstClock *clock = gst_system_clock_obtain();
    GstAlarmSrc* self = GST_ALARM_SRC_CAST (src);
    int stream_num = 0;
    switch (self->connection) {
        case GST_ALARM_IPC_CONNECTION_VIDEO_S1:
        {
            if (FALSE == self->initialized) {
                printf("!!! gst_alarm_src_create, connection %d initialized %d\n", self->connection, self->initialized);
                self->src_ptr = init_video_plugin();
                open_video_stream(self->src_ptr, stream_num);
                self->initialized = TRUE;
            }

            ret = get_video_frame(self->src_ptr, &frame, &fp);
            if (ret == -1) {
                return GST_FLOW_OK; //todo error ret?
            }

            *buffer = gst_buffer_new_and_alloc ((size_t) fp.size);
            g_assert (gst_buffer_map (*buffer, &info, GST_MAP_WRITE));
            memcpy (info.data, fp.data, fp.size);
            gst_buffer_unmap (*buffer, &info);
            release_video_frame(self->src_ptr, frame);

            timestamp = gst_util_uint64_scale (fp.pts, 1, 1);
            //printf("conn 0 TIMESTAMP %llu pts %llu\n", timestamp, fp.pts);

            /* SPS/PPS/VPS and IDR may have same seqnum */
            if (gst_util_seqnum_compare (fp.seq, self->last_fp_seqnum) < 0 ||
                gst_util_seqnum_compare (fp.seq, self->last_fp_seqnum) > 1)
                gst_buffer_set_flags (*buffer, GST_BUFFER_FLAG_DISCONT);
            self->last_fp_seqnum = fp.seq;

        break;
        }
        case GST_ALARM_IPC_CONNECTION_VIDEO_S2:
        {
#ifdef MOCK
            ret = get_video_mock_clip_frame(&fp);
            printf("!!! %u, %u, %llu, %u \n", fp.size, fp.type, fp.pts, fp.seq);
#else
            if (FALSE == self->initialized) {
                printf("!!! gst_alarm_src_create, connection %d initialized %d\n", self->connection, self->initialized);
                //stream 1 already opened with video Init()
                self->initialized = TRUE;
            }
            ret = get_video_clip_frame(self->src_ptr, &fp);
#endif

            if (ret == -1) {
                //printf("!!! null !!! \n");
                return GST_FLOW_EOS;
            }

            if (ret == 1) {
                printf("!!! end of clip !!! \n");
                return GST_FLOW_EOS;
            }

            *buffer = gst_buffer_new_and_alloc ((size_t) fp.size);
            g_assert (gst_buffer_map (*buffer, &info, GST_MAP_WRITE));
            memcpy (info.data, fp.data, fp.size);
            gst_buffer_unmap (*buffer, &info);
#ifdef MOCK
            release_video_mock_clip_frame((void *) fp.data);
            timestamp = gst_util_uint64_scale (fp.pts, 1000, 1);
#else
            release_video_clip_frame(self->src_ptr, (void *) fp.data);
            timestamp = gst_util_uint64_scale (fp.pts, 1000, 1);
#endif
        break;
        }
        case GST_ALARM_IPC_CONNECTION_VIDEO_S3:
        {   
            break;
        }
        case GST_ALARM_IPC_CONNECTION_AUDIO_IN:
        {
            if (FALSE == self->initialized) {
                printf("Audio Initalize\n\n");
                self->list = gst_buffer_list_new ();
                self->src_ptr_a = init_audio_plugin(self);
                g_usleep(10000);
                start_audio_capture(self->src_ptr_a);
                self->initialized = TRUE;      
            }

            do {
                if (gst_buffer_list_length (self->list) == 0) {
                    g_usleep(50000);
                } else 
                    break;
            } while(1);

        #if 0
            *buffer = gst_buffer_list_get(self->list, 0);
            //printf("Address asas %p\n", *buff);
            //gst_buffer_list_remove(self->list, 0, 1);
            //GstBuffer *buff1 = gst_buffer_list_get(self->list, 0);
            //printf("Address asas-- %p\n", *buff1);
        #endif
            *buffer = gst_buffer_copy(gst_buffer_list_get(self->list, 0));
            gst_buffer_list_remove(self->list, 0, 1);

        }
        case GST_ALARM_IPC_CONNECTION_AUDIO_OUT:
        {   
            break;
        }
        case GST_ALARM_IPC_CONNECTION_EVENT:
        {   
            break;
        }
    }

    if (!GST_CLOCK_TIME_IS_VALID (self->internal_base_time)) {
        GST_DEBUG_OBJECT (self, "Internal base time set to %"GST_TIME_FORMAT, GST_TIME_ARGS (timestamp));
        self->internal_base_time = timestamp;
        self->wallclock = gst_clock_get_time(clock);
    }

    /**
     * Live sources should generate timestamps as absolute_time - base_time,
     * IPC provides us timestamps so we use first frame timestamp as base_time
     * and timestamp of each frame as absolute_time
     */
    GST_BUFFER_TIMESTAMP (*buffer) = timestamp - self->internal_base_time;
    //printf("!!! Buffer Timestamp: %llu,  Running Time: %llu Diff: %llu \n", (timestamp - self->internal_base_time), (gst_clock_get_time(clock) - self->wallclock),
                //(timestamp - self->internal_base_time) - (gst_clock_get_time(clock) - self->wallclock));

    if ((timestamp - self->internal_base_time) > (gst_clock_get_time(clock) - self->wallclock))
        g_usleep(((timestamp - self->internal_base_time) - (gst_clock_get_time(clock) - self->wallclock))/1000);

    /* Treat audio, jpeg and IDR as key frames */
    gst_buffer_set_flags (*buffer, GST_BUFFER_FLAG_DELTA_UNIT);

    switch (fp.type) {
        case SPS:
        /* fall through */
        case PPS:
        /* fall through */
        case VPS:
        gst_buffer_set_flags (*buffer, GST_BUFFER_FLAG_HEADER);
        /* fall through */
        case AUDIO:
        /* fall through */
        case JPEG:
        gst_buffer_unset_flags (*buffer, GST_BUFFER_FLAG_DELTA_UNIT);
        break;
        default:
        break;
    }

    GST_DEBUG_OBJECT (self,
        "Outputting buffer %p with timestamp %" GST_TIME_FORMAT " and duration %"
        GST_TIME_FORMAT" size %d", *buffer, GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (*buffer)),
        GST_TIME_ARGS (GST_BUFFER_DURATION (*buffer)), gst_buffer_get_size(*buffer));

    return GST_FLOW_OK;
}

/* GstElement vmethod implementations */
static GstStateChangeReturn gst_alarm_src_change_state (GstElement * element, GstStateChange transition)
{
    GstAlarmSrc *self = GST_ALARM_SRC_CAST (element);
    //printf("!!! gst_alarm_src_change_state, connection %d\n", self->connection);
    GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

    switch (transition) {
        case GST_STATE_CHANGE_NULL_TO_READY:
            //printf("GST_STATE_CHANGE_NULL_TO_READY\n");
        break;
        case GST_STATE_CHANGE_READY_TO_PAUSED:
            //printf("GST_STATE_CHANGE_READY_TO_PAUSED\n");
            ret = GST_STATE_CHANGE_NO_PREROLL;
        break;
        default:
        break;
    }

    if (ret == GST_STATE_CHANGE_FAILURE)
        return ret;
    ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
    if (ret == GST_STATE_CHANGE_FAILURE)
        return ret;

    switch (transition) {
        case GST_STATE_CHANGE_PAUSED_TO_READY:
            //printf("GST_STATE_CHANGE_PAUSED_TO_READY\n");
        break;
        case GST_STATE_CHANGE_PLAYING_TO_PAUSED: {
            //printf("GST_STATE_CHANGE_PLAYING_TO_PAUSED\n");
            GST_DEBUG_OBJECT (self, "Stopping stream");
            gst_alarm_src_stop (self);
            ret = GST_STATE_CHANGE_NO_PREROLL;
        break;
        }
        case GST_STATE_CHANGE_PAUSED_TO_PLAYING: {
            //printf("GST_STATE_CHANGE_PAUSED_TO_PLAYING\n");
            GST_DEBUG_OBJECT (self, "Starting stream");
            gst_alarm_src_start (self);
        break;
        }
        case GST_STATE_CHANGE_READY_TO_NULL:
            //printf("GST_STATE_CHANGE_READY_TO_NULL\n");
        break;
        default:
        break;
    }

  return ret;
}
