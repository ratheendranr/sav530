/*
 * Copyright ©
 */

#include "gstalarmsrc.h"

static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "alarmsrc", GST_RANK_NONE,
      GST_TYPE_ALARM_SRC))
    return FALSE;

  return TRUE;
}

#ifndef PACKAGE
#define PACKAGE "alarm"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    alarm,
    "alarm by VXG Inc.",
    plugin_init,
    "no version",
    "LGPL",
    "GStreamer",
    "http://vxg.io/"
)
