/**
 * @file test-plugin-video.cc
 * @brief video plugin test
 * @author Stephanie Sohn <ssohn@alarm.com>
 * @date 2020-05-04
 *
 *
 */
#if 0
#include <loader.h>
#include <video_plugin.h>
#include "logger.h"
#include "dictionary.h"
#include "iniparser.h"

using namespace cosmos::apps;
//extern int my_argc;
//extern char** my_argv;

FILE *fp[ST_MAX_STREAM_NUM];
FILE *mfp[ST_MAX_STREAM_NUM];

#define MAX_SENSOR_NUM 2
#define MAX_PORT_NUM 5

void StreamCallback(int stream_id, std::shared_ptr<VideoData> data) {
    std::string sType;
    switch (data->type)
    {
        case 1 : sType = "PSLICE"; break;
        case 5 : sType = "ISLICE"; break;
        case 6 : sType = "SEI"; break;
        case 7 : sType = "SPS"; break;
        case 8 : sType = "PPS"; break;
        case 9 : sType = "IPSLICE"; break;
        default : sType = "UNKONWN TYPE";
    }

    fwrite(data->data.get(),data->data_len, 1, fp[stream_id]);
    fprintf(mfp[stream_id], "%u,%u,%llu,%u\n", data->data_len, data->type, data->pts, data->seq);
}
#endif
void test_cosmos_media_plugin()
{
#if 0
    //LOG

    using namespace g3;
    std::unique_ptr<LogWorker> logworker{ LogWorker::createLogWorker() };
    auto sinkHandle = logworker->addSink(std::make_unique<LogRotate>("test-plugin-video", "/tmp"),
                                              &LogRotate::save);
    logworker->addSink(std::make_unique<cosmos::apps::CosmosLogSink>(),
                                   &cosmos::apps::CosmosLogSink::ReceiveLogMessage);

    auto syslog_sink = std::make_unique<SyslogSink>("test-plugin-video");
    syslog_sink->setFormatter(&cosmos::apps::logger_syslog_format);
    logworker->addSink(std::move(syslog_sink), &SyslogSink::syslog);

    initializeLogging(logworker.get());
    sinkHandle->call(&LogRotate::setMaxLogSize, 3*1024);
    sinkHandle->call(&LogRotate::setMaxArchiveLogCount, 5);

    only_change_at_initialization::addLogLevel(ERROR, true);


    //PLUGIN

    PluginManager plugin_mgr_;
    IPluginFactory *info_p = plugin_mgr_.addPlugin("/adc/alarm_video/",
            "libcosmosplugin-video");
    if (!info_p)
        throw "could not add video plugin";

    std::string video_plugin_path("/adc/alarm_video/");
    video_plugin_path.append("libcosmosplugin-video");

    auto video_ = plugin_mgr_.CreateInstanceAs<VideoPlugin> (video_plugin_path, "VideoPlugin");

    std::string video_plugin_name(video_->Name());
    LOG(INFO) << "Registered video plugin: " << video_plugin_name;


    //INIT FILE PROCESSING

    char filename[100]={0,};
    dictionary *pstDict;
    int numSensorStreams = 0, sensorNum;
    VideoConfiguration * vidConfig;
    char buffer[128] = {0};
    ImageSettings_t imageSettings;
    ImageSettings_t * imageSettingsPtr;

    //LOG(INFO) << "Command line args count: " << my_argc;

    //if (my_argc < 2)
    //{
    //    LOG(ERROR) << "Provide an initialization file";
    //    return;
    //}

    for (int file_iter = 1; file_iter < 2 ; file_iter++)
    {
        //LOG(INFO) << "Reading initialization file: " << my_argv[file_iter];
        //pstDict = iniparser_load(my_argv[file_iter]);
       /*
        sensorNum = iniparser_getint(pstDict, ":sensorNum", 0);
        imageSettings.brightness_level = iniparser_getint(pstDict, ":brightness", 0);
        imageSettings.contrast_level = iniparser_getint(pstDict, ":contrast", 0);
        imageSettings.saturation_level = iniparser_getint(pstDict, ":saturation", 0);
        imageSettings.sharpness_level = iniparser_getint(pstDict, ":sharpness", 0);
        imageSettings.flipimage = bool(iniparser_getint(pstDict, ":flipimage", 0));
        imageSettings.mirrorimage = bool(iniparser_getint(pstDict, ":mirrorimage", 0));
        imageSettings.hdr = bool(iniparser_getint(pstDict, ":hdr", 0));
        imageSettings.ldc_file = iniparser_getstring(pstDict, ":ldcFile", (char*) "NULL");
        imageSettings.flicker = iniparser_getint(pstDict, ":flicker", 0);
        imageSettings.night_mode = iniparser_getint(pstDict, ":nightMode", 0);
        imageSettings.expo_comp_ev = iniparser_getint(pstDict, ":expo_comp_ev", 0);
        imageSettings.expo_comp_grad = iniparser_getint(pstDict, ":expo_comp_grad", 0);
        imageSettingsPtr = &imageSettings;
        video_->SetImageSettings(sensorNum, imageSettingsPtr);

        numSensorStreams = iniparser_getint(pstDict, ":numStreams", 1);
        for (int port_iter = 0; port_iter < numSensorStreams; port_iter++)
        {
            vidConfig = new VideoConfiguration;
            vidConfig->running_ = true;
            sprintf(buffer, ":codec%d", port_iter);
            vidConfig->codec_ = (VideoConfiguration::Codec) iniparser_getint(pstDict, buffer, 1);
            sprintf(buffer, ":profile%d", port_iter);
            vidConfig->profile_ = (VideoConfiguration::Profile) iniparser_getint(pstDict, buffer, 1);
            sprintf(buffer, ":fps%d", port_iter);
            vidConfig->fps_ = (unsigned int) iniparser_getint(pstDict, buffer, 30);
            sprintf(buffer, ":ratecontrol%d", port_iter);
            vidConfig->bctrl_ = (VideoConfiguration::RateControl) iniparser_getint(pstDict, buffer, 1);
            sprintf(buffer, ":gop%d", port_iter);
            vidConfig->gop_ = (unsigned int) iniparser_getint(pstDict, buffer, 30);
            sprintf(buffer, ":width%d", port_iter);
            vidConfig->width_ = (unsigned int) iniparser_getint(pstDict, buffer, 1920);
            sprintf(buffer, ":height%d", port_iter);
            vidConfig->height_ = (unsigned int) iniparser_getint(pstDict, buffer, 1080);
            sprintf(buffer, ":bitrate%d", port_iter);
            vidConfig->bitrate_ = (unsigned int) iniparser_getint(pstDict, buffer, 2698615);
            video_->InitStreamConfig(sensorNum*MAX_PORT_NUM+port_iter, vidConfig);
        }*/
    }

    for (int stream_config_iter = 0; stream_config_iter < MAX_SENSOR_NUM * MAX_PORT_NUM; stream_config_iter++)
    {
        if (! video_->StreamUsed(stream_config_iter))
            continue;

        video_->PrintStreamConfig(stream_config_iter);

        sprintf (filename, "/adc/alarm_video/video_stream_%d.h264", stream_config_iter);
        fp[stream_config_iter]= fopen(filename, "wb");
        if (NULL == fp[stream_config_iter])
        {
            LOG(ERROR) << "Failed to open file " << filename;
            return;
        }

        sprintf (filename, "/adc/alarm_video/meta_video_stream_%d.txt", stream_config_iter);
        mfp[stream_config_iter] = fopen(filename, "w");
        if (NULL == mfp[stream_config_iter])
        {
            LOG(ERROR) << "Failed to open file " << filename;
            return;
        }
    }


    //SETUP PIPELINE
    video_->Init();
    video_->RegisterCallback(StreamCallback);

    //START PIPELINE
    video_->Start();
    std::this_thread::sleep_for(std::chrono::seconds(20));

/*
    video_->EncoderStandby(0);
    std::this_thread::sleep_for(std::chrono::seconds(5));
    video_->EncoderResume(0);

    video_->EncoderStandby(1);
    std::this_thread::sleep_for(std::chrono::seconds(5));
    video_->EncoderResume(1);

    video_->EncoderStandby(2);
    std::this_thread::sleep_for(std::chrono::seconds(5));
    video_->EncoderResume(2);
*/
    //video_->SetImageSetting(0, ImageSettings::brightness, 100); //works
    //video_->SetImageSetting(0, ImageSettings::contrast, 100); //doesnt work - probably needs calibration
    //video_->SetImageSetting(0, ImageSettings::saturation, 100); //doesnt work - probably needs calibration
    //video_->SetImageSetting(0, ImageSettings::nightmode, 1); //grayscale works, array doesnt - probably needs calibration
    //video_->SetImageSetting(0, ImageSettings::flicker, 2); //not working?, struct value not even changing
    //video_->SetImageSetting(0, ImageSettings::expo_comp_ev, 100); //works
    //video_->SetImageSetting(0, ImageSettings::expo_comp_grad, 100); //cant tell if works visually
    //video_->SetImageSetting(0, ImageSettings::flipimage, 1);
    //video_->SetImageSetting(0, ImageSettings::mirrorimage, 1);
    /* above 2 work but output error:
    [MI ERR ]: MI_SYS_IMPL_EnsureInputPortFifoEmpty[456]: Next input(mod7, dev0, pass0, chn0, port0, reject_c, fifo_cnt:0 0 1 0 0) no response in 1000ms!
    [MI ERR ]: MI_SYS_IMPL_EnsureInputPortFifoEmpty[456]: Next input(mod7, dev0, pass0, chn0, port0, reject_c, fifo_cnt:0 0 1 0 0) no response in 1000ms!
    [MI ERR ]: _MI_VPE_CheckVifMask[623]: isp input change but vif is unmask, check 10ms *100 fail, u16CheckC 0, force mask
    */
    //video_->SetBitrate(0, 50000); //works
    //video_->SetQuality(0, 0); //cant see a difference

    std::this_thread::sleep_for(std::chrono::seconds(20));

    //STOP PIPELINE
    video_->Stop();

    for (int stream_config_iter = 0; stream_config_iter < MAX_SENSOR_NUM * MAX_PORT_NUM; stream_config_iter++)
    {
        if (! video_->StreamUsed(stream_config_iter))
            continue;

        fclose(fp[stream_config_iter]);
        fclose(mfp[stream_config_iter]);
    }
#endif
}
