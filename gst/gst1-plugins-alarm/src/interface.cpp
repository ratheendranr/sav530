#include "interface.h"
#include <loader.h>
#include <video_plugin.h>
#include <audio_plugin.h>
#include "logger.h"
#include "log_init.h"
#include <memory>

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <thread>
#include "gstalarmsrc.h"
#include <gst/gst.h>

#ifdef __cplusplus
extern "C" {
#endif

using namespace g3;
using namespace cosmos::apps;

GType gst_alarm_ipc_connection_get_type (void)
{
    static gsize id = 0;
    static const GEnumValue connections[] = {
    {GST_ALARM_IPC_CONNECTION_VIDEO_S1, "Video S1", "/dev/ipc/stream/1"},
    {GST_ALARM_IPC_CONNECTION_VIDEO_S2, "Video S2", "/dev/ipc/stream/2"},
    {GST_ALARM_IPC_CONNECTION_VIDEO_S3, "Video S3", "/dev/ipc/stream/3"},
    {GST_ALARM_IPC_CONNECTION_AUDIO_IN, "Audio input", "/dev/ipc/ai/1"},
    {GST_ALARM_IPC_CONNECTION_AUDIO_OUT, "Audio output", "/dev/ipc/ao/1"},
    {GST_ALARM_IPC_CONNECTION_EVENT, "Event", "/dev/ipc/ev.s"},
    {0, NULL, NULL}
  };

    if (g_once_init_enter (&id)) {
        GType tmp = g_enum_register_static ("GstAlarmIPCConnection", connections);
        g_once_init_leave (&id, tmp);
    }

    return (GType) id;
}

void* init_video_plugin()
{
    static VideoPlugin v = [] {
        if(!g3::internal::isLoggingInitialized()) {
            std::unique_ptr<g3::LogWorker> logworker = LogInit("alarmsrc-video", 0, nullptr);
        }

        auto p = VideoPlugin();
        printf("Creating video plugin pointer %p \n", &p);
        p.Init();
        //p.SetImageSetting(0, ImageSettings::flipimage, 1);
        return p;
    } ();

    printf("Returning video plugin pointer %p \n", &v);
    return &v;
}

int init_mock()
{
    printf("!!! init_mock\n");
    tvfp = fopen("/adc/video_stream_0.h264", "rb");
    tmvfp = fopen("/adc/meta_video_stream_0.txt", "r");
    int i = 0;
    if (tvfp && tmvfp) {
        printf("!!! could open files\n");
        while(fgets(line[i], LSIZ, tmvfp))
        {
            line[i][strlen(line[i]) - 1] = '\0';
            i++;
        }
    }
    else {
        printf("!!! Could not open files /adc/video_stream_0.h264 and /adc/meta_video_stream_0.txt\n");
        return 1;
    }
    curline = 0;
    return 0;
}

void open_video_stream(void * video, int stream)
{
    ((VideoPlugin*)video)->OpenStream(stream);
}

void close_video_stream(void * video, int stream)
{
    ((VideoPlugin*)video)->CloseStream(stream);
}

int get_video_frame(void * video, void ** frame, fp_frm_t * fp)
{
    MI_VENC_Stream_t * stream = nullptr;
    stream = (MI_VENC_Stream_t *) ((VideoPlugin*)video)->GetFrame(0); //stream/venc channel is 0
    *frame = stream;
    fp->size = stream->pstPack[0].u32Len; //not sure needs offset
    fp->data = stream->pstPack[0].pu8Addr; //not sure needs offset
    fp->pts = stream->pstPack[0].u64PTS;
    fp->seq = stream->u32Seq;
    fp->codec = H264; //todo via src instead?
    switch (stream->pstPack[0].stDataType.eH264EType) //todo based on codec
    {
        case 1 : fp->type = PSLICE; break;
        case 5 : fp->type = ISLICE; break;
        case 6 : fp->type = SEI; break;
        case 7 : fp->type = SPS; break;
        case 8 : fp->type = PPS; break;
        case 9 : fp->type = IPSLICE; break;
        default : return -1;
    };
    
    return 0;
}

int get_video_mock_clip_frame(fp_frm_t * fp)
{
    if (curline >= RSIZ ) {
        printf("curline ending %d\n", curline);
        return 1;
    }

    char *tokenptr;
    const char delimiter[2] = ",";

    tokenptr = strtok(line[curline], delimiter);
    fp->size = atoi(tokenptr);

    tokenptr = strtok(NULL, delimiter);
    int type = atoi(tokenptr);

    tokenptr = strtok(NULL, delimiter);
    fp->pts = strtoull(tokenptr, NULL, 10);

    tokenptr = strtok(NULL, delimiter);
    fp->seq = strtoul(tokenptr, NULL, 10);

    fp->codec = H264; //todo via src instead?

    switch (type) //todo based on codec
    {
        case 1 : fp->type = PSLICE; break;
        case 5: fp->type = ISLICE; break;
        default : return -1;
    };
    printf("!!! %u, %u, %llu, %u \n", fp->size, fp->type, fp->pts, fp->seq);

    fp->data = (unsigned char*) calloc( 1, fp->size + 1);
    if( !fp->data )  {
        printf("calloc error fp->data\n");
        return -1;
    }
    int rcount = fread(fp->data, fp->size, 1, tvfp);
    if( 1 != rcount )  {
        printf("fread error fp->data\n");
        return -1;
    }
    //printf("Data read from file 0: %x \n", fp->data[0]);
    //printf("Data read from file last: %x \n", fp->data[fp->size -1 ]);
    curline = curline + 1;

    return 0;
}

int get_video_clip_frame(void * video, fp_frm_t * fp)
{
    uint32_t len = 0;
    uint64_t pts = 0;
    uint8_t * data = nullptr;
    bool clipDone;

    if (0 == eventTimeStamp) {
        eventTimeStamp = std::time(0);
#ifdef H264_DUMP
        std::string filename("/adc/clip_");
        filename.append(std::to_string(eventTimeStamp));
        filename.append(".h264");
        write_ptr = fopen(filename.c_str(),"wb");
#endif
        ((VideoPlugin*)video)->StartNewClip(eventTimeStamp);
    }

    do {
        data = (uint8_t *) ((VideoPlugin*)video)->GetClipFrame(&clipDone, &len, &pts);
        //printf("frame len %u, pts %llu\n", len,  pts) ;
        if (data == nullptr) {
            ((VideoPlugin*)video)->ReleaseClipFrame(data);
            sleep(1);
        }
    } while(clipDone == false && data == nullptr);

    if (clipDone == true) {
        eventTimeStamp = 0;
#ifdef H264_DUMP
        fclose(write_ptr);
#endif
        return 1; //use to return eos in create
    }

    if (data == nullptr)
        return -1;

    fp->size = len;
    fp->data = data;
    fp->pts = pts;
    fp->seq = 0; //todo?
    fp->codec = H264;
    fp->type = ISLICE;
#ifdef H264_DUMP
    fwrite(fp->data,fp->size,1,write_ptr);
#endif

    return 0;
}

void release_video_frame(void * video, void * frame)
{
    ((VideoPlugin*)video)->ReleaseFrame(frame, 0);
    frame = nullptr;

    #if 0
        audio->ReleaseFrame(frame, 0);
        frame = nullptr;
    #endif
}

void AiDataCallback(void *data, int length, int channel, void *arg)
{
    GstAlarmSrc *src = (GstAlarmSrc*) arg;
    GstMapInfo info;

    GstBuffer *buffer = gst_buffer_new_and_alloc((size_t)length);
    g_assert(gst_buffer_map(buffer, &info, GST_MAP_WRITE));
    memcpy(info.data, (char*)data, length);
    gst_buffer_unmap(buffer, &info);

    gst_buffer_list_add(src->list, buffer);
}

void* init_audio_plugin(void *src) 
{
    bool init_done = false;

    static AudioPlugin aud = [](void *src, bool *done) {
        if(!g3::internal::isLoggingInitialized()) {
            std::unique_ptr<g3::LogWorker> logworker = LogInit("alarmsrc-audio", 0, nullptr);
        }
        auto audio = AudioPlugin();
        LOG(INFO) << "Creating audio plugin pointer";
        audio.Init(AudioConfiguration::Mode::kAudioInput);
        audio.RegisterAiCallback(AiDataCallback, 0, src);
        *done = true;
        return audio;
    } (src, &init_done);

    if (!init_done) {
        LOG(INFO) << "initializing audio";
        aud.Init(AudioConfiguration::Mode::kAudioInput);
        aud.RegisterAiCallback(AiDataCallback, 0, src);
    }

    return &aud;
}

int deinit_audio_plugin(void *audio)
{
    AudioPlugin *audio_p = (AudioPlugin*)audio;
    LOG(INFO) << "Deinit AUDIO CAPTURE";

    if (AudioPlugin::Result::kOK != audio_p->DeInit()) {
        LOG(ERROR) << "ERROR de-initializing audio capture";
        return -1;
    }

    if (AudioPlugin::Result::kOK != audio_p->UnRegisterAiCallback(0)) {
        LOG(ERROR) << "Failed to unregister the callback";
        return -1;
    }

    return 0;
}

int start_audio_capture(void *audio)
{
    AudioPlugin *audio_p = (AudioPlugin*)audio;
    LOG(INFO) << "Starting audio capture";

    if (AudioPlugin::Result::kOK != audio_p->StartAudioCapture()) {
        LOG(ERROR)  << "Error starting audio capture";
        return -1;
    }
    return 0;
}

int stop_audio_capture(void *audio)
{
    AudioPlugin *audio_p = (AudioPlugin*)audio;
    LOG(INFO) << "Stopping audio capture";

    if (AudioPlugin::Result::kOK != audio_p->StopAudioCapture()) {
       LOG(ERROR)  << "Error stopping audio capture";
        return -1;
    }

    return 0;
}

int get_audio_frame(void *audio, void **frame, fp_frm_t *fp)
{
    AudioPlugin *aud = (AudioPlugin*) audio;
    MI_AUDIO_Frame_t *stream_a = nullptr;
    int ret = 0;

    stream_a = (MI_AUDIO_Frame_t*) aud->GetFrame(0, &ret);
    if (ret < 0) {
        LOG(ERROR) << "Failed to get audio frame";
    }

    *frame = stream_a;
    fp->size = stream_a->u32Len;
    fp->data = (unsigned char*) stream_a->apVirAddr[0];
    fp->pts = stream_a->u64TimeStamp;
    fp->seq = stream_a->u32Seq;
    fp->codec = G711U;
    fp->type = AUDIO;
    
    return 0;
}

void release_audio_frame(void *audio, void *frame)
{
    AudioPlugin *aud = (AudioPlugin*) audio;
    aud->ReleaseFrame(frame, 0);
}

void release_video_mock_clip_frame(void * data)
{
    printf("!!! release mock clip frame\n") ;
    free(data);
}

void release_video_clip_frame(void * video, void * data)
{
    //printf("!!! release clip frame\n") ;
    ((VideoPlugin*)video)->ReleaseClipFrame(data);
}

#ifdef __cplusplus
}
#endif
