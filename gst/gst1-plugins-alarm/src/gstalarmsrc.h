/*
 * Copyright ©
 */

#ifndef __GST_ALARM_SRC_H__
#define __GST_ALARM_SRC_H__

#include <gst/gst.h>
#include <gst/base/gstbasesrc.h>
#include <gst/base/gstpushsrc.h>
#include <gst/base/gstqueuearray.h>
#include "interface.h"
#include <stdint.h>

G_BEGIN_DECLS

#define GST_TYPE_ALARM_SRC \
  (gst_alarm_src_get_type())
#define GST_ALARM_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ALARM_SRC,GstAlarmSrc))
#define GST_ALARM_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ALARM_SRC,GstAlarmSrcClass))
#define GST_IS_ALARM_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ALARM_SRC))
#define GST_IS_ALARM_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ALARM_SRC))
#define GST_ALARM_SRC_CAST(obj) ((GstAlarmSrc*) obj)

typedef struct _GstAlarmSrc      GstAlarmSrc;
typedef struct _GstAlarmSrcClass GstAlarmSrcClass;

struct _GstAlarmSrc
{
  GstPushSrc parent;
  void *src_ptr;
  void *src_ptr_a;
  GstAlarmIPCConnectionEnum connection;
  size_t last_fp_seqnum;
  GstClockTime internal_base_time;
  gboolean initialized;
  uint64_t wallclock;
  GstBufferList *list;
};

struct _GstAlarmSrcClass
{
  GstPushSrcClass parent_class;
};

GType gst_alarm_src_get_type (void);

G_END_DECLS

#endif /* __GST_ALARM_SRC_H__ */
