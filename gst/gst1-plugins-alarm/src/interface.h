#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <stdint.h>

G_BEGIN_DECLS

typedef enum {
  GST_ALARM_IPC_CONNECTION_VIDEO_S1, /* /dev/ipc/stream/1 */
  GST_ALARM_IPC_CONNECTION_VIDEO_S2, /* /dev/ipc/stream/2 */
  GST_ALARM_IPC_CONNECTION_VIDEO_S3, /* /dev/ipc/stream/3 */

  GST_ALARM_IPC_CONNECTION_AUDIO_IN, /* /dev/ipc/ai/1 */
  GST_ALARM_IPC_CONNECTION_AUDIO_OUT,/* /dev/ipc/ao/1 */

  GST_ALARM_IPC_CONNECTION_EVENT,    /* /dev/ipc/ev.s */
} GstAlarmIPCConnectionEnum;


#define GST_TYPE_ALARM_IPC_CONNECTION (gst_alarm_ipc_connection_get_type ())
GType gst_alarm_ipc_connection_get_type (void);

typedef enum {
    PSLICE = 0,
    ISLICE,
    SEI,
    SPS,
    PPS,
    IPSLICE,
    VPS,
    JPEG,
    AUDIO,
    UNKNOWN
} FrameType;

typedef enum {
    NONE = 0,
    MJPEG,
    H264,
    H265,
    G711A, //todo not sure correct?
    G711U,
} CodecType;

typedef struct
{
    CodecType codec;
    FrameType type;
    unsigned char* data;
    unsigned int seq;
    unsigned long long pts;
    size_t size;
} fp_frm_t;

G_END_DECLS

uint64_t eventTimeStamp;
#ifdef H264_DUMP
FILE *write_ptr;
#endif

int init_mock();
void* init_video_plugin();
void open_video_stream(void * video, int stream);
void close_video_stream(void * video, int stream);
int get_video_frame(void * video, void ** frame, fp_frm_t * fp);
int get_video_clip_frame(void * video, fp_frm_t * fp);
int get_video_mock_clip_frame(fp_frm_t * fp);
void release_video_frame(void * video, void * frame);
void release_video_clip_frame(void * video, void * data);
void release_video_mock_clip_frame(void * data);

#define LSIZ 100
#define RSIZ 100

char line[RSIZ][LSIZ];
FILE * tvfp;
FILE * tmvfp;
int curline;
int curindex;

void* init_audio_plugin(void *src);
int deinit_audio_plugin(void *audio);
int start_audio_capture(void *audio);
int stop_audio_capture(void *audio);
int get_audio_frame(void *audio, void **frame, fp_frm_t *fp);
void release_audio_frame(void *audio, void *frame);
void AiDataCallback(void *data, int length, int channel, void *arg);

#ifdef __cplusplus
}
#endif

#endif /* __INTERFACE_H__ */
