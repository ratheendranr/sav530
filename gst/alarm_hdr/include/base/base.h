#ifndef __BASE_H__
#define __BASE_H__
#include <stdbool.h>
#include <stdint.h>

typedef enum {
    SU_FRM_TYPE_PSLICE = 0,
    SU_FRM_TYPE_ISLICE,
    SU_FRM_TYPE_VIRTUAL_ISLICE,
    SU_FRM_TYPE_SEI,
    SU_FRM_TYPE_SPS,
    SU_FRM_TYPE_PPS,
    SU_FRM_TYPE_IPSLICE,
    SU_FRM_TYPE_VPS,
    SU_FRM_TYPE_JPEG,
    SU_FRM_TYPE_AUDIO
} SU_FRM_TYPE_T;

typedef enum {
    //source
    SU_EV_TYPE_DI = 0,
    SU_EV_TYPE_MD,
    SU_EV_TYPE_NETFAIL,
    SU_EV_TYPE_MANUAL,
    SU_EV_TYPE_PERIODIC,
    SU_EV_TYPE_SDERR,
    SU_EV_TYPE_COLDBOOT,
    SU_EV_TYPE_OVERLIGHT,
    SU_EV_TYPE_OVERDARK,
    SU_EV_TYPE_REDIRECTION,
    SU_EV_TYPE_COLORCAST,
    SU_EV_TYPE_DEFOCUS,
    SU_EV_TYPE_BLOCKAGE,
    SU_EV_TYPE_WIFINETFAIL,
    SU_EV_TYPE_AUDIO_LOUD,
    SU_EV_TYPE_TRIPWIRE,
    SU_EV_TYPE_INTRUSION,
    SU_EV_TYPE_HUMAN,
    SU_EV_TYPE_CUSTOM,
    SU_EV_TYPE_CUSTOM2,
    SU_EV_TYPE_SOURCE_END,
    SU_EV_TYPE_RECORDING_JOBSTATE,
    SU_EV_TYPE_RECORDING_RECORDINGJOBCONFIGURATION,
    SU_EV_TYPE_RECORDING_TRACKCONFIGURATION,
    SU_EV_TYPE_RECORDING_RECORDINGCONFIGURATION,
    SU_EV_TYPE_LENS_ZOOM_NG,
    SU_EV_TYPE_LENS_FOCUS_NG,
    SU_EV_TYPE_LENS_IRIS_NG,
    //SU_EV_TYPE_TEST,
    //action
    SU_EV_TYPE_DO = 0x10000
} SU_EV_TYPE_T;

typedef enum {
    SU_CODEC_NONE = 0,
    SU_CODEC_MJPEG,
    SU_CODEC_H264,
    SU_CODEC_H265,
    SU_CODEC_VIDEO_END,

    SU_CODEC_G711A = 0x10,
    SU_CODEC_G711U,
    SU_CODEC_G726_16K,
    SU_CODEC_G726_24K,
    SU_CODEC_G726_32K,
    SU_CODEC_G726_40K,
    SU_CODEC_AAC,
    SU_CODEC_PCM48K_16_CH1,
    SU_CODEC_PCM08K_16_CH1,
    SU_CODEC_AUDIO_END,

    SU_CODEC_EVENT_IVA = 0x50,
    SU_CODEC_EVENT_END
} SU_CODEC_TYPE_T;

typedef struct
{
    SU_CODEC_TYPE_T codec;
    SU_FRM_TYPE_T type;
    char magic[4];
    unsigned int seq;
    unsigned long long pts;
    unsigned int offset;
    size_t size;
} fp_frm_t;

typedef struct
{
    //char uuid[37];
    int type;
    int id;
    time_t time;
    int value;
} su_ev_alarm_t;

#endif //__BASE_H__
