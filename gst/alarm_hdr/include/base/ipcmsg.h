// IPC message definitions
#ifndef __IPCMSG_H__
#define __IPCMSG_H__
#ifdef __cplusplus
extern "C" {
#endif
#include <netinet/in.h>

// 0x7xxx is a command set that are necessary to response
#define IPCMSG_TYPE_REQUEST_AUDIO_OUT                  0x0001   // handled by /dev/ipc/ao/#, and resply the ipcmsg_resp_code struct
#define IPCMSG_TYPE_REQUEST_AUDIO_OUT_RTSP                  0x0002  
#define IPCMSG_TYPE_REQUEST_AUDIO_OUT_ADC                  0x0003

// 0x8xxx is a command set that are not necessary to response
#define IPCMSG_TYPE_DUMMY                       0x8000 // conistent interface for receiver if sender is not necessary to send any request
#define IPCMSG_TYPE_HAL_CLIENT_CONNECT         0x8001
#define IPCMSG_TYPE_HAL_CLIENT_DISCONNECT      0x8002
#define IPCMSG_TYPE_HAL_VAD_MOTION       0x4005

#define IPCMSG_TYPE_HAL_3AD_IMG_COLOR    0x4001
#define IPCMSG_TYPE_HAL_VAD_TAMPERING    0x4006
#define IPCMSG_TYPE_HAL_VAD_VA           0x4007
#define IPCMSG_TYPE_HAL_3AD_EVENT_DN_TRIG         0x4008
#define IPCMSG_TYPE_HAL_3AD_EVENT_DN_FIXED_POS    0x4009

#define IPCMSG_TYPE_RESPONSE_CODE               0x8fff
#define IPCMSG_TYPE_NONE                        0xffff

typedef struct ipcmsg_hdr
{
    unsigned short type;
    unsigned short length;
} ipcmsg_hdr_t;

enum {
    IPCMSG_RESP_CODE_OK = 0,
    IPCMSG_RESP_CODE_FAIL,
    IPCMSG_RESP_CODE_NOMEM,
    IPCMSG_RESP_CODE_FORMAT_ERR,
    IPCMSG_RESP_CODE_BUTT,
};

typedef struct ipcmsg_resp_code {
    unsigned int code;
} ipcmsg_resp_code_t;


#define IPCMSG_TRANSPORT_TYPE_UDP          0x1
#define IPCMSG_TRANSPORT_TYPE_TCP          0x2
#define IPCMSG_TRANSPORT_TYPE_HTTP         0x3
#define IPCMSG_TRANSPORT_TYPE_MULTICAST    0x4
// for message type IPCMSG_TYPE_HAL_CLIENT_CONNECT, IPCMSG_TYPE_HAL_CLIENT_DISCONNECT
typedef struct ipcmsg_hal_report {
    unsigned int session;
    unsigned char ipfamily; // AF_INET, AF_INET6
    unsigned char transport;    // udp, tcp, http, multicast
    union {
        struct {
            struct sockaddr_in  addr;
        }ipv4;
        struct {
            struct sockaddr_in6 addr;
        }ipv6;
    }net;
} ipcmsg_hal_report_t;

// IPCMSG_TYPE_REQUEST_AUDIO_OUT
// ipcmsg_hdr.length is strlen of url after the ipcmsg_hdr
// such as url is lan://localhost:80/1/audio/out/1/aac, length is 37 including null terminator


#define HAL_3AD_EVENT_DN_TRIG               0x1
#define HAL_3AD_EVENT_DN_FIXED_POS_INFO     0x2


#ifdef __cplusplus
}
#endif
#endif
